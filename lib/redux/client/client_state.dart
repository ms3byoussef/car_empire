// ignore_for_file: prefer_collection_literals

import '../../data/models/car_model.dart';
import '../../data/models/request_model.dart';
import '../../data/models/review_model.dart';
import '../../data/models/user_model.dart';
import '../action_report.dart';

class ClientState {
  Map<String, ActionReport> status;
  RequestModel? request;
  CarModel? requestCar;
  final Map<String, RequestModel> requests;
  ReviewModel? review;
  final Map<String, ReviewModel> reviews;
  UserModel? requestUser;
  UserModel? carOwnerUser;
  UserModel? reviewUser;
  final UserModel? currentUser;
  final Map<String, UserModel>? clients;

  ClientState({
    required this.status,
    required this.request,
    required this.requests,
    required this.review,
    required this.reviews,
    required this.requestUser,
    required this.reviewUser,
    this.carOwnerUser,
    this.requestCar,
    required this.currentUser,
    this.clients,
  });

  factory ClientState.initial() {
    return ClientState(
      status: Map(),
      request: null,
      requestCar: null,
      requests: Map(),
      review: null,
      carOwnerUser: null,
      reviews: Map(),
      requestUser: null,
      reviewUser: null,
      currentUser: null,
      clients: Map(),
    );
  }

  ClientState copyWith({
    Map<String, ActionReport>? status,
    UserModel? requestUser,
    UserModel? reviewUser,
    UserModel? currentUser,
    UserModel? carOwnerUser,
    RequestModel? request,
    Map<String, RequestModel>? requests,
    ReviewModel? review,
    CarModel? requestCar,
    Map<String, ReviewModel>? reviews,
    final Map<String, UserModel>? clients,
  }) {
    return ClientState(
      status: status ?? this.status,
      requestUser: requestUser ?? this.requestUser,
      carOwnerUser: carOwnerUser ?? this.carOwnerUser,
      currentUser: currentUser ?? this.currentUser,
      request: request ?? this.request,
      requestCar: requestCar ?? this.requestCar,
      requests: requests ?? this.requests,
      review: review ?? this.review,
      reviews: reviews ?? this.reviews,
      reviewUser: reviewUser ?? this.reviewUser,
      clients: clients ?? this.clients,
    );
  }
}
