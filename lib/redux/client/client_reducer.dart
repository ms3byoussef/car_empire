// ignore_for_file: avoid_print

import 'package:redux/redux.dart';

import 'client_actions.dart';
import 'client_state.dart';

final clientReducer = combineReducers<ClientState>([
  TypedReducer<ClientState, ClientStatusAction>(_orderAuthState),
  TypedReducer<ClientState, SyncRequestAction>(_syncRequest),
  TypedReducer<ClientState, SyncRequestUserAction>(_syncRequestUser),
  TypedReducer<ClientState, SyncReviewUserAction>(_syncReviewUser),
  // TypedReducer<ClientState, SyncOrderProductAction>(_syncRequestCar),
  TypedReducer<ClientState, SyncRequestsAction>(_syncRequests),
  TypedReducer<ClientState, RemoveRequestAction>(_syncRemoveRequest),
  TypedReducer<ClientState, SyncReviewAction>(_syncReview),
  TypedReducer<ClientState, SyncReviewsAction>(_syncReviews),
  TypedReducer<ClientState, SyncRequestUserAction>(_syncRequestUser),
  TypedReducer<ClientState, SyncCarOwnerCompanyAction>(_syncCarOwnerUser),
  TypedReducer<ClientState, RemoveReviewAction>(_syncRemoveReview),
  TypedReducer<ClientState, SyncClientsAction>(_syncClients),
]);

ClientState _orderAuthState(ClientState state, ClientStatusAction action) {
  var status = state.status;
  status.update(action.report.actionName!, (v) => action.report,
      ifAbsent: () => action.report);

  return state.copyWith(status: status);
}

ClientState _syncRequest(ClientState state, SyncRequestAction action) {
  state.requests.update(
      action.request!.id.toString(), (request) => action.request ?? request,
      ifAbsent: () => action.request!);

  return state.copyWith(requests: state.requests);
}

ClientState _syncRequests(ClientState state, SyncRequestsAction action) {
  for (var request in action.requests ?? []) {
    state.requests.update(request.id.toString(), (request) => request,
        ifAbsent: () => request);
  }

  return state.copyWith(requests: state.requests);
}

ClientState _syncRequestUser(ClientState state, SyncRequestUserAction action) {
  state.requestUser = action.user;

  return state.copyWith(requestUser: action.user);
}

ClientState _syncReviewUser(ClientState state, SyncReviewUserAction action) {
  state.reviewUser = action.user;

  return state.copyWith(reviewUser: action.user);
}

// ClientState _syncOrderProduct(
//     ClientState state, SyncOrderProductAction action) {
//   state.orderProduct = action.product;

//   return state.copyWith(orderProduct: action.product);
// }

ClientState _syncRemoveRequest(ClientState state, RemoveRequestAction action) {
  state.requests.remove(action.request!);
  return state.copyWith(requests: state.requests);
}

ClientState _syncReview(ClientState state, SyncReviewAction action) {
  state.reviews.update(
      action.review!.id.toString(), (review) => action.review ?? review,
      ifAbsent: () => action.review!);

  return state.copyWith(reviews: state.reviews);
}

ClientState _syncReviews(ClientState state, SyncReviewsAction action) {
  for (var review in action.reviews ?? []) {
    state.reviews.update(review.id.toString(), (review) => review,
        ifAbsent: () => review);
  }

  return state.copyWith(reviews: state.reviews);
}

ClientState _syncRemoveReview(ClientState state, RemoveReviewAction action) {
  state.reviews.remove(action.review!.id.toString());
  return state.copyWith(reviews: state.reviews);
}

// ignore: unused_element
ClientState _syncOrderUser(ClientState state, SyncRequestUserAction action) {
  state.requestUser = action.user;

  return state.copyWith(requestUser: action.user);
}

ClientState _syncCarOwnerUser(
    ClientState state, SyncCarOwnerCompanyAction action) {
  state.carOwnerUser = action.user;

  return state.copyWith(requestUser: action.user);
}

ClientState _syncClients(ClientState state, SyncClientsAction action) {
  for (var client in action.clients ?? []) {
    state.clients!.update(client.id.toString(), (client) => client,
        ifAbsent: () => client);
  }

  return state.copyWith(clients: state.clients);
}
