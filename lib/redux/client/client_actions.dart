import 'package:car_empire/data/models/user_info.dart';

import '../../data/models/car_model.dart';
import '../../data/models/request_model.dart';
import '../../data/models/review_model.dart';
import '../../data/models/user_model.dart';
import '../action_report.dart';

class ClientStatusAction {
  final String actionName = "ClientStatusAction";
  final ActionReport report;
  ClientStatusAction({required this.report});
}

class PostRequestAction {
  final String actionName = "PostRequestAction";
  final RequestModel? request;

  PostRequestAction({this.request});
}

class GetRequestUserAction {
  final String actionName = "GetRequestUserAction";
  final RequestModel? request;
  GetRequestUserAction({this.request});
}

class SyncRequestUserAction {
  final String actionName = "SyncRequestUserAction";
  final UserModel? user;
  SyncRequestUserAction({this.user});
}

class UpdateUserInfoAction {
  final String actionName = "UpdateUserInfoAction";
  final UserInformation userInformation;

  UpdateUserInfoAction({required this.userInformation});
}

class GetCarOwnerCompanyAction {
  final String actionName = "GetCarOwnerCompanyAction";
  final CarModel? car;
  GetCarOwnerCompanyAction({this.car});
}

class SyncCarOwnerCompanyAction {
  final String actionName = "SyncCarOwnerCompanyAction";
  final UserModel? user;
  SyncCarOwnerCompanyAction({this.user});
}

class GetRequestCarAction {
  final String actionName = "GetRequestCarAction";
  RequestModel? car;
  GetRequestCarAction({this.car});
}

class SyncCarInRequestAction {
  final String actionName = "SyncCarInRequestAction";
  final CarModel? car;
  SyncCarInRequestAction({this.car});
}

class SyncRequestAction {
  final String actionName = "SyncRequestAction";
  final RequestModel? request;
  SyncRequestAction({this.request});
}

class GetRequestsAction {
  final String actionName = "GetRequestsAction";
  GetRequestsAction();
}

class SyncRequestsAction {
  final String actionName = "SyncRequestsAction";
  final List<RequestModel>? requests;

  SyncRequestsAction({this.requests});
}

class UpdateRequestAction {
  final String actionName = "UpdateRequestAction";
  final String? requestID;
  final String? status;
  UpdateRequestAction({this.requestID, this.status});
}

class UpdateCarStatusAction {
  final String actionName = "UpdateCarStatusAction";
  final String? carId;
  final String? status;
  UpdateCarStatusAction({this.carId, this.status});
}

class RemoveRequestAction {
  final String actionName = "RemoveRequestAction";

  final RequestModel? request;

  RemoveRequestAction({
    this.request,
  });
}

class PostReviewAction {
  final String actionName = "PostReviewAction";
  final ReviewModel? review;
  PostReviewAction({this.review});
}

class GetReviewUserAction {
  final String actionName = "GetReviewUserAction";
  final ReviewModel? review;
  GetReviewUserAction({this.review});
}

class SyncReviewUserAction {
  final String actionName = "SyncReviewUserAction";
  final UserModel? user;
  SyncReviewUserAction({this.user});
}

class SyncReviewAction {
  final String actionName = "SyncReviewAction";
  final ReviewModel? review;
  SyncReviewAction({this.review});
}

class GetReviewsAction {
  final String actionName = "GetReviewsAction";
  GetReviewsAction();
}

class SyncReviewsAction {
  final String actionName = "SyncReviewsAction";
  final List<ReviewModel>? reviews;

  SyncReviewsAction({this.reviews});
}

class RemoveReviewAction {
  final String actionName = "RemoveReviewAction";

  final ReviewModel? review;

  RemoveReviewAction({
    this.review,
  });
}

class GetClientsAction {
  final String actionName = "GetClientsAction";
  final List<UserModel>? clients;
  GetClientsAction({this.clients});
}

class SyncClientsAction {
  final String actionName = "SyncClientsAction";
  final List<UserModel>? clients;
  SyncClientsAction({this.clients});
}
