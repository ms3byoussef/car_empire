// ignore_for_file: avoid_function_literals_in_foreach_calls, avoid_print, no_leading_underscores_for_local_identifiers

import 'package:redux/redux.dart';

import '../../data/repository/client_repository.dart';
import '../action_report.dart';
import '../app/app_state.dart';
import '../auth/auth_actions.dart';
import '../car/car_actions.dart';
import 'client_actions.dart';

List<Middleware<AppState>> createClientMiddleware([
  ClientRepository _repository = const ClientRepository(),
]) {
  final postRequest = _postRequest(_repository);
  final getRequestUser = _getRequestUser(_repository);
  final getRequestCar = _getRequestCar(_repository);
  final updateUserInfo = _updateUserInfo(_repository);

  final getRequests = _getRequests(_repository);
  final updateRequest = _updateRequest(_repository);
  final updateCarStatus = _updateCarStatus(_repository);
  final removeRequest = _removeRequest(_repository);
  final postReview = _postReview(_repository);
  final getReviewUser = _getReviewUser(_repository);
  final getCarOwnerCompany = _getCarOwnerCompany(_repository);

  final getReviews = _getReviews(_repository);
  final getClients = _getClients(_repository);
  final removeReview = _removeReview(_repository);

  return [
    TypedMiddleware<AppState, PostRequestAction>(postRequest),
    TypedMiddleware<AppState, GetRequestUserAction>(getRequestUser),
    TypedMiddleware<AppState, GetRequestUserAction>(getCarOwnerCompany),
    TypedMiddleware<AppState, GetRequestCarAction>(getRequestCar),
    TypedMiddleware<AppState, GetReviewUserAction>(getReviewUser),
    TypedMiddleware<AppState, UpdateUserInfoAction>(updateUserInfo),
    TypedMiddleware<AppState, GetRequestsAction>(getRequests),
    TypedMiddleware<AppState, UpdateRequestAction>(updateRequest),
    TypedMiddleware<AppState, UpdateCarStatusAction>(updateCarStatus),
    TypedMiddleware<AppState, RemoveRequestAction>(removeRequest),
    TypedMiddleware<AppState, PostReviewAction>(postReview),
    TypedMiddleware<AppState, GetReviewsAction>(getReviews),
    TypedMiddleware<AppState, GetClientsAction>(getClients),
    TypedMiddleware<AppState, RemoveReviewAction>(removeReview),
  ];
}

Middleware<AppState> _postRequest(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .postRequest(
      action.request,
    )
        .then((request) {
      next(SyncRequestAction(request: request));
      completed(next, action);
    });
  };
}

Middleware<AppState> _getRequests(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    store.state.clientState!.requests.clear();
    running(next, action);
    repository.getRequests().then((requests) {
      next(SyncRequestsAction(requests: requests));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getRequestUser(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getRequestUser(action.request).then((user) {
      next(SyncRequestUserAction(user: user));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getCarOwnerCompany(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getCarOwnerUser(action.car).then((user) {
      next(SyncCarOwnerCompanyAction(user: user));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getRequestCar(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getCarInRequest(action.request).then((car) {
      action.request.car = car;

      next(SyncCarInRequestAction(car: car));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _updateUserInfo(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .updateUserInfo(
      action.userInformation,
    )
        .then((user) {
      next(SyncUserAction(user));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getReviewUser(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getReviewUser(action.review).then((user) {
      next(SyncReviewUserAction(user: user));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _updateRequest(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .updateRequest(requestID: action.requestID, status: action.status)
        .then((request) {
      next(SyncRequestAction(request: request));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _updateCarStatus(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .updateCarStatus(carID: action.carId, status: action.status)
        .then((car) {
      next(SyncCarAction(car: car));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _removeRequest(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .removeRequest(
      action.request,
    )
        .then((request) {
      next(action);
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _postReview(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    // print('test');
    repository
        .postReview(
      action.review,
    )
        .then((review) {
      next(SyncReviewAction(review: review));
      print(review.id);
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getReviews(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    store.state.clientState!.requests.clear();
    repository.getReviews().then((reviews) {
      next(SyncReviewsAction(reviews: reviews));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getClients(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.getClients().then((clients) {
      next(SyncClientsAction(clients: clients));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _removeReview(ClientRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .removeReview(
      action.reviewId,
    )
        .then((order) {
      next(action);
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

void catchError(NextDispatcher next, action, error) {
  next(ClientStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: error.toString())));
}

void completed(NextDispatcher next, action) {
  next(ClientStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.complete,
          msg: "${action.actionName} is completed")));
}

void running(NextDispatcher next, action) {
  next(ClientStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.running,
          msg: "${action.actionName} is running")));
}

void idEmpty(NextDispatcher next, action) {
  next(ClientStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: "Id is empty")));
}
