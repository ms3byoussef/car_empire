import '../../data/models/car_model.dart';
import '../../data/models/user_model.dart';
import '../action_report.dart';

class CarState {
  Map<String, ActionReport> status;
  CarModel? car;
  Map<String, CarModel> cars;
  Map<String, CarModel> categoryCars;
  List<CarModel>? searchCar;
  UserModel? user;

  CarState(
      {required this.status,
      required this.car,
      required this.cars,
      required this.categoryCars,
      required this.user,
      this.searchCar});

  factory CarState.initial() {
    return CarState(
      car: null,
      user: null,
      status: {},
      cars: {},
      categoryCars: {},
      searchCar: [],
    );
  }

  CarState copyWith({
    UserModel? user,
    CarModel? car,
    Map<String, ActionReport>? status,
    Map<String, CarModel>? cars,
    Map<String, CarModel>? categoryCars,
    List<CarModel>? searchCar,
  }) {
    return CarState(
      car: car ?? this.car,
      user: user ?? this.user,
      status: status ?? this.status,
      cars: cars ?? this.cars,
      categoryCars: categoryCars ?? this.categoryCars,
      searchCar: searchCar ?? this.searchCar,
    );
  }
}
