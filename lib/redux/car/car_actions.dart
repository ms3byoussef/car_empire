import 'dart:io';
import '../../data/models/car_model.dart';
import '../action_report.dart';

class CarStatusAction {
  final String actionName = "CarStatusAction";
  final ActionReport report;
  CarStatusAction({required this.report});
}

class PostCarAction {
  final String actionName = "PostCarAction";
  final File? img;
  final CarModel? car;

  PostCarAction({
    this.img,
    this.car,
  });
}

class SyncCarAction {
  final String actionName = "SyncCarAction";
  final CarModel? car;

  SyncCarAction({this.car});
}

class SyncCarsAction {
  final String actionName = "SyncCarsAction";
  final List<CarModel>? cars;

  SyncCarsAction({this.cars});
}

class GetCarsAction {
  final String actionName = "GetCarsAction";

  GetCarsAction();
}

class GetCategoryCarsAction {
  final String actionName = "GetCategoryCarsAction";
  final String category;
  GetCategoryCarsAction({
    required this.category,
  });
}

class SyncCategoryCarsAction {
  final String actionName = "SyncCategoryCarsAction";
  final List<CarModel>? categoryCars;

  SyncCategoryCarsAction({this.categoryCars});
}

class GetCarAction {
  final String actionName = "GetCarAction";

  final String? carId;

  GetCarAction({this.carId});
}

class EditCarAction {
  final String actionName = "EditCarAction";

  final CarModel? car;
  final File? img;
  EditCarAction({this.car, this.img});
}

class SearchCarAction {
  final String actionName = "SearchCarAction";

  final String searchWord;

  SearchCarAction({
    required this.searchWord,
  });
}

class SyncSearchCarAction {
  final String actionName = "SyncSearchCarAction";

  List<CarModel>? searchCar;
  SyncSearchCarAction({this.searchCar});
}

class RemoveCarAction {
  final String actionName = "RemoveCarAction";

  final String? carId;
  RemoveCarAction({this.carId});
}
