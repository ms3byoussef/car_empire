import 'package:redux/redux.dart';

import 'car_actions.dart';
import 'car_state.dart';

final carReducer = combineReducers<CarState>([
  TypedReducer<CarState, CarStatusAction>(_companyAuthState),
  TypedReducer<CarState, SyncCarAction>(_syncCar),
  TypedReducer<CarState, SyncCarsAction>(_syncCars),
  TypedReducer<CarState, SyncCategoryCarsAction>(_syncCategoryCars),
  TypedReducer<CarState, RemoveCarAction>(_syncRemoveCar),
  TypedReducer<CarState, SyncSearchCarAction>(_syncSearchCar),
]);

CarState _companyAuthState(CarState state, CarStatusAction action) {
  var status = state.status;
  status.update(action.report.actionName!, (v) => action.report,
      ifAbsent: () => action.report);

  return state.copyWith(status: status);
}

CarState _syncCar(CarState state, SyncCarAction action) {
  state.cars.update(action.car!.id.toString(), (car) => action.car!,
      ifAbsent: () => action.car!);

  return state.copyWith(car: action.car);
}

CarState _syncCars(CarState state, SyncCarsAction action) {
  for (var car in action.cars ?? []) {
    state.cars.update(car.id.toString(), (cars) => cars, ifAbsent: () => car);
  }

  return state.copyWith(cars: state.cars);
}

CarState _syncCategoryCars(CarState state, SyncCategoryCarsAction action) {
  for (var car in action.categoryCars ?? []) {
    state.categoryCars
        .update(car.id.toString(), (cars) => cars, ifAbsent: () => car);
  }

  return state.copyWith(categoryCars: state.categoryCars);
}

CarState _syncRemoveCar(CarState state, RemoveCarAction action) {
  state.cars.remove(action.carId.toString());
  return state.copyWith(cars: state.cars);
}

CarState _syncSearchCar(CarState state, SyncSearchCarAction action) {
  state.searchCar = action.searchCar;

  return state.copyWith(searchCar: state.searchCar);
}
