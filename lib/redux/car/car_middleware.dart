// ignore_for_file: avoid_print, no_leading_underscores_for_local_identifiers

import 'package:redux/redux.dart';

import '../../data/repository/car_repository.dart';
import '../action_report.dart';
import '../app/app_state.dart';
import 'car_actions.dart';

List<Middleware<AppState>> createCarMiddleware([
  CarRepository _repository = const CarRepository(),
]) {
  final postCar = _postCar(_repository);
  final getCars = _getCars(_repository);
  final getCategoryCars = _getCategoryCars(_repository);
  final editCar = _editCar(_repository);
  final removeCar = _removeCar(_repository);
  final searchCar = _searchCar(_repository);

  return [
    TypedMiddleware<AppState, PostCarAction>(postCar),
    TypedMiddleware<AppState, GetCarsAction>(getCars),
    TypedMiddleware<AppState, GetCategoryCarsAction>(getCategoryCars),
    TypedMiddleware<AppState, EditCarAction>(editCar),
    TypedMiddleware<AppState, RemoveCarAction>(removeCar),
    TypedMiddleware<AppState, SearchCarAction>(searchCar),
  ];
}

Middleware<AppState> _postCar(CarRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    // print('test');
    repository
        .postCar(
      action.car,
      action.img,
    )
        .then((car) {
      next(SyncCarAction(car: car));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getCars(CarRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    store.state.carState!.cars.clear();
    repository.getCars().then((cars) {
      next(SyncCarsAction(cars: cars));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _getCategoryCars(CarRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    store.state.carState!.categoryCars.clear();
    repository.getCategoryCars(action.category).then((categoryCars) {
      next(SyncCategoryCarsAction(categoryCars: categoryCars));
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _editCar(CarRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.editCar(action.car, img: action.img).then((car) {
      next(SyncCarAction(car: car));
      completed(next, action);
    });

    // .catchError((error) {
    //   print(error);
    //   catchError(next, action, error);
    // });
  };
}

Middleware<AppState> _removeCar(CarRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository
        .removeCar(
      action.carId,
    )
        .then((car) {
      next(action);
      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

Middleware<AppState> _searchCar(CarRepository repository) {
  return (Store<AppState> store, dynamic action, NextDispatcher next) {
    running(next, action);
    repository.searchCarsResult(action.searchWord).then((car) {
      next(SyncSearchCarAction(searchCar: car));

      completed(next, action);
    }).catchError((error) {
      print(error);
      catchError(next, action, error);
    });
  };
}

void catchError(NextDispatcher next, action, error) {
  next(CarStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: error.toString())));
}

void completed(NextDispatcher next, action) {
  next(CarStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.complete,
          msg: "${action.actionName} is completed")));
}

void running(NextDispatcher next, action) {
  next(CarStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.running,
          msg: "${action.actionName} is running")));
}

void idEmpty(NextDispatcher next, action) {
  next(CarStatusAction(
      report: ActionReport(
          actionName: action.actionName,
          status: ActionStatus.error,
          msg: "Id is empty")));
}
