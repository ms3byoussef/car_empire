import '../auth/auth_state.dart';
import '../client/client_state.dart';
import '../car/car_state.dart';

class AppState {
  final AuthState? authState;
  final CarState? carState;
  final ClientState? clientState;

  AppState({this.authState, this.carState, this.clientState});
  factory AppState.initial() {
    return AppState(
      authState: AuthState.initial(),
      carState: CarState.initial(),
      clientState: ClientState.initial(),
    );
  }
}
