import '../auth/auth_reducer.dart';
import '../client/client_reducer.dart';
import '../car/car_reducer.dart';
import 'app_state.dart';

AppState appReducer(AppState state, dynamic action) {
  return AppState(
    authState: authReducer(state.authState!, action),
    carState: carReducer(state.carState!, action),
    clientState: clientReducer(state.clientState!, action),
  );
}
