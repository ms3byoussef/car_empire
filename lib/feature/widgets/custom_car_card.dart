import 'package:car_empire/utils/app_theme.dart';
import 'package:flutter/material.dart';

import '../../data/models/car_model.dart';
import '../presentations/users_screens/components/car_details_screen/car_details_screen.dart';

class CustomCarCard extends StatelessWidget {
  final CarModel? car;

  const CustomCarCard({
    this.car,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5),
      child: SizedBox(
        height: 130,
        child: Card(
          child: InkWell(
            splashColor: const Color(0XFFAAAEB4),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return CarDetailsScreen(
                      car: car!,
                    );
                  },
                ),
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                FadeInImage.assetNetwork(
                    width: 140,
                    height: 120,
                    fit: BoxFit.contain,
                    placeholderCacheHeight: 50,
                    placeholderCacheWidth: 50,
                    placeholder: 'assets/images/loading.gif',
                    image: car!.carImage!),
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        car!.title!,
                        style: AppTheme.blackText,
                      ),
                      Text(
                        car!.category ?? "",
                        style: AppTheme.hintText.copyWith(fontSize: 16),
                      ),
                      Text(
                        '${car!.price}/KWD',
                        style: AppTheme.primaryText,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(
                            Icons.car_rental,
                            color: AppTheme.primaryColor,
                          ),
                          Icon(
                            Icons.car_crash_outlined,
                            color: AppTheme.primaryColor,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                RotatedBox(
                  quarterTurns: -1,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return CarDetailsScreen(
                              car: car!,
                            );
                          },
                        ),
                      );
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppTheme.primaryColor,
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                        ),
                      ),
                    ),
                    child: Text(
                      "Request",
                      style: AppTheme.blackHeadline
                          .copyWith(color: Colors.white, fontSize: 18),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
