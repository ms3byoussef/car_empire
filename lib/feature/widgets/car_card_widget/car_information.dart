import 'package:flutter/material.dart';

import '../../../data/models/car_model.dart';
import '../../../utils/app_theme.dart';
import 'attribute.dart';

class CarInformation extends StatelessWidget {
  const CarInformation({
    Key? key,
    @required this.car,
  }) : super(key: key);

  final CarModel? car;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(left: 24, right: 24, top: 50),
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          // gradient: const LinearGradient(
          //   colors: [Color(0xff3c80f7), Color(0xff1058d1)],
          //   stops: [0, 1],
          //   begin: Alignment(-1.00, 0.00),
          //   end: Alignment(1.00, -0.00),
          //   // angle: 90,
          //   // scale: undefined,
          // ),
          color: AppTheme.primaryColor),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('\$${car!.price}',
              style: AppTheme.blackHeadline.copyWith(color: Colors.white)),
          Text(
            'KWD',
            style: AppTheme.whiteText,
          ),
          const SizedBox(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Attribute(
                value: car!.category ?? "",
                name: 'Category',
              ),
              // Attribute(
              //   value: car.model,
              //   name: 'Model No',
              // ),
              // Attribute(
              //   value: car.co2,
              //   name: 'CO2',
              // ),
              // Attribute(
              //   value: car.fuelCons,
              //   name: 'Fuel Cons.',
              // ),
            ],
          )
        ],
      ),
    );
  }
}
