import 'package:flutter/material.dart';

import '../../../data/models/car_model.dart';
import '../../presentations/users_screens/components/car_details_screen/car_details_screen.dart';
import 'car_information.dart';

class CarCard extends StatelessWidget {
  const CarCard({
    this.car,
    Key? key,
  }) : super(key: key);

  final CarModel? car;

  @override
  Widget build(BuildContext context) {
    // Car car = carList[index];
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) {
              return CarDetailsScreen(
                car: car!,
              );
            },
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 20),
        child: Stack(
          children: [
            CarInformation(
              car: car,
            ),
            Positioned(
              right: 40,
              child: Hero(
                tag: car!.id,
                child: car!.carImage!.isNotEmpty
                    ? FadeInImage.assetNetwork(
                        width: 120,
                        height: 120,
                        fit: BoxFit.contain,
                        placeholderCacheHeight: 10,
                        placeholderCacheWidth: 10,
                        placeholder: 'assets/images/loading.gif',
                        image: car!.carImage!)
                    : Image.asset("assets/images/loading.gif"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
