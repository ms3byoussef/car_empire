// ignore_for_file: use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/svg.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../data/models/request_model.dart';
import '../../redux/action_report.dart';
import '../../redux/app/app_state.dart';
import '../../utils/app_theme.dart';
import '../../utils/progress_dialog.dart';
import '../presentations/users_screens/client_screens/client_view_model.dart';
import 'icon_with_text.dart';

class RequestCard extends StatelessWidget {
  final RequestModel? request;

  const RequestCard({
    this.request,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      builder: (_, viewModel) => _RequestCard(
        viewModel: viewModel,
        request: request,
      ),
      converter: (store) {
        return ClientViewModel.fromStore(store);
      },
    );
  }
}

class _RequestCard extends StatefulWidget {
  final ClientViewModel? viewModel;
  final RequestModel? request;
  const _RequestCard({Key? key, this.request, this.viewModel})
      : super(key: key);

  @override
  __RequestCardState createState() => __RequestCardState();
}

class __RequestCardState extends State<_RequestCard> {
  @override
  void initState() {
    widget.viewModel!.getRequestUser!(widget.request!);
    widget.viewModel!.getCarOwnerUser!(widget.request!.car!);

    super.initState();
  }

  ProgressDialog? progressDialog;

  @override
  void didUpdateWidget(_RequestCard oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel!.removeRequestReport?.status ==
          ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (progressDialog!.isShowing()) {
          progressDialog!.setMessage("Cancel Order ...");
          progressDialog!.show();
        }
      } else {
        if (widget.viewModel!.removeRequestReport?.status ==
            ActionStatus.error) {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
          showTopSnackBar(
            context as OverlayState,
            CustomSnackBar.error(
              message: widget.viewModel!.removeRequestReport!.msg.toString(),
            ),
          );
        } else if (widget.viewModel!.removeRequestReport?.status ==
            ActionStatus.complete) {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }

          widget.viewModel!.removeRequestReport?.status = null;
        } else {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
        }
      }
      widget.viewModel!.removeRequestReport?.status = null;
    });
  }

  deleteRequest() {
    widget.viewModel!.removeRequest!(widget.request!);
    showTopSnackBar(
      context as OverlayState,
      CustomSnackBar.error(
        message: "Delate the Request",
        textStyle: AppTheme.blackHeadline.copyWith(color: Colors.white),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return widget.request!.car != null
        ? Container(
            margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            padding: const EdgeInsets.symmetric(horizontal: 12),
            height: 140,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(14)),
              boxShadow: [
                BoxShadow(
                    color: Color(0x29000000),
                    offset: Offset(0, 3),
                    blurRadius: 6,
                    spreadRadius: 0)
              ],
              color: Color(0xffffffff),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                    onTap: () {},
                    child: Image.network(
                      widget.request!.car!.carImage.toString(),
                      width: 90,
                      fit: BoxFit.contain,
                    )),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                        padding: const EdgeInsets.symmetric(vertical: 6),
                        child: Text(widget.request!.car!.title.toString(),
                            maxLines: 2, style: AppTheme.cardText)),
                    Text.rich(
                      TextSpan(children: [
                        TextSpan(
                          text: widget.request!.car!.price.toString(),
                        ),
                        const TextSpan(text: "KWD"),
                      ], style: AppTheme.hintText),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6),
                      child: widget.viewModel!.requestUser != null &&
                              widget.viewModel!.currentUser!.role != "client"
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                RowWTextAndIcon(
                                  text: widget.viewModel!.requestUser!.name,
                                  style: AppTheme.cardText,
                                  icon: const Icon(Icons.person),
                                ),
                                RowWTextAndIcon(
                                  text: widget.viewModel!.requestUser!.phone,
                                  style: AppTheme.cardText,
                                  icon: const Icon(Icons.phone),
                                ),
                              ],
                            )
                          : Container(),
                    ),
                  ],
                ),
                Opacity(
                  opacity: .7,
                  child: InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (_) => AlertDialog(
                          content: const Text("Is this Request accepted ?"),
                          title: const Text(" done ?"),
                          actions: [
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                                showTopSnackBar(
                                  context as OverlayState,
                                  const CustomSnackBar.success(
                                      message:
                                          "this Request Not accepted yet!"),
                                );
                              },
                              child: const Text("No"),
                            ),
                            ElevatedButton(
                                onPressed: () async {
                                  widget.viewModel!.updateRequest!(
                                      requestID: widget.request!.id,
                                      status: "accepted");
                                  widget.viewModel!.updateCarStatus!(
                                      carId: widget.request!.car!.id,
                                      carStatus: "inProgress");
                                  showTopSnackBar(
                                    context as OverlayState,
                                    const CustomSnackBar.info(
                                      message: "this Request is  accepted ",
                                    ),
                                  );
                                  Navigator.pop(context);
                                },
                                child: const Text("yes"))
                          ],
                          elevation: 24,
                        ),
                      );
                    },
                    child: widget.viewModel!.currentUser!.id ==
                            widget.request!.car!.ownerID
                        ? Row(
                            children: [
                              SvgPicture.asset(
                                "assets/icons/check.svg",
                                height: 35,
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                            ],
                          )
                        : Column(
                            children: [
                              (widget.request!.status == 'accepted')
                                  ? Image.asset(
                                      "assets/images/approved.png",
                                      height: 80,
                                    )
                                  : widget.request!.status == 'rejected'
                                      ? Image.asset(
                                          "assets/images/rejected.png",
                                          height: 80,
                                          fit: BoxFit.cover,
                                        )
                                      : Image.asset(
                                          "assets/images/wait.png",
                                          height: 80,
                                          fit: BoxFit.contain,
                                        ),
                            ],
                          ),
                  ),
                ),
                Opacity(
                  opacity: .7,
                  child: InkWell(
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                          content: const Text("Do you Cancel This Request ?"),
                          title: const Text("cancel"),
                          actions: [
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                                showTopSnackBar(
                                  context as OverlayState,
                                  const CustomSnackBar.success(
                                      message: "your Request is not Canceled"),
                                );
                              },
                              child: const Text("No"),
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    widget.viewModel!
                                        .removeRequest!(widget.request!);
                                    Navigator.pop(context);
                                  });

                                  showTopSnackBar(
                                    context as OverlayState,
                                    const CustomSnackBar.info(
                                      message: "your Request is Canceled",
                                    ),
                                  );
                                },
                                child: const Text("Yes"))
                          ],
                          elevation: 24,
                        ),
                      );
                    },
                    child: SvgPicture.asset(
                      "assets/icons/cancel.svg",
                    ),
                  ),
                ),
              ],
            ),
          )
        : Container();
  }
}
