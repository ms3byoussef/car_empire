// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

import '../../utils/app_theme.dart';

class CustomDropDown extends StatefulWidget {
  String? label;
  String? hintText;
  TextEditingController? controller = TextEditingController();
  List<String> dropDownList;
  TextStyle? labelTextStyle = AppTheme.whiteText;

  CustomDropDown({
    this.controller,
    this.label,
    this.hintText,
    this.labelTextStyle,
    required this.dropDownList,
    Key? key,
  }) : super(key: key);

  @override
  State<CustomDropDown> createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(
          height: 20,
        ),
        Text(widget.label!, style: widget.labelTextStyle),
        const SizedBox(height: 10.0),
        Container(
          height: 55,
          width: double.infinity,
          decoration: kBoxDecorationStyle,
          alignment: Alignment.centerLeft,
          child: DropdownButtonFormField(
              hint: Text(
                widget.hintText!,
                style: AppTheme.whiteText,
              ),
              alignment: AlignmentDirectional.center,
              isDense: true,
              isExpanded: true,
              // value: widget.controller!.text,
              style: AppTheme.whiteText,
              elevation: 4,
              borderRadius: BorderRadius.circular(10),
              dropdownColor: Colors.blueGrey,
              icon: const Icon(
                Icons.keyboard_arrow_down_outlined,
                color: Colors.white,
                size: 35,
              ),
              items: widget.dropDownList
                  .map<DropdownMenuItem<String>>(
                      (String e) => DropdownMenuItem<String>(
                            value: e,
                            child: Text(e),
                          ))
                  .toList(),
              onChanged: (String? newValue) {
                setState(() {
                  widget.controller!.text = newValue!;
                  // print(newValue);
                });
              }),
        ),
      ],
    );
  }
}
