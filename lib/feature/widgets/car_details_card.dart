import 'package:flutter/material.dart';

class CarDetailsCard extends StatelessWidget {
  final String title;
  final String subTitle;
  final Widget iconsOrText;
  const CarDetailsCard(
      {Key? key,
      required this.title,
      required this.subTitle,
      required this.iconsOrText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      width: 150,
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 8),
      padding: const EdgeInsets.symmetric(horizontal: 30),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(31),
        gradient: const LinearGradient(
          colors: [Colors.white10, Colors.white],
          stops: [0, 1],
          begin: Alignment(-1.00, 0.50),
          end: Alignment(1.00, -0.00),
          // angle: 90,
          // scale: undefined,
        ),
      ),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            iconsOrText,
            const SizedBox(
              height: 10,
            ),
            RichText(
              text: TextSpan(
                style: const TextStyle(
                  color: Colors.black87,
                ),
                children: <TextSpan>[
                  TextSpan(text: title),
                  TextSpan(
                    text: subTitle,
                    style: const TextStyle(
                      color: Colors.black87,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
