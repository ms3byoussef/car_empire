// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import '../../utils/app_theme.dart';
import '../presentations/users_screens/components/search_screen/search_screen.dart';

class SearchBar extends StatelessWidget {
  final void Function()? onTap;
  const SearchBar({
    Key? key,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: Colors.white,
      ),
      child: TextFormField(
        scrollPadding: const EdgeInsets.symmetric(horizontal: 5),
        onTap: onTap,
        keyboardType: TextInputType.text,
        style: AppTheme.whiteText
            .copyWith(color: AppTheme.primaryColor, fontSize: 16),
        decoration: InputDecoration(
          border: InputBorder.none,
          enabledBorder: InputBorder.none,
          hintText: "search",
          // contentPadding:
          //     const EdgeInsets.symmetric(vertical: 6, horizontal: 8),
          hintStyle: AppTheme.hintText
              .copyWith(color: AppTheme.primaryColor, fontSize: 16),
          suffixIcon: IconButton(
            icon: Icon(Icons.search_rounded, color: AppTheme.primaryColor),
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => const SearchScreen()));
            },
          ),
        ),
      ),
    );
  }
}
