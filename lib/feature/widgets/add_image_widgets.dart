// ignore_for_file: must_be_immutable

import 'dart:io';

import 'package:flutter/material.dart';

import '../../utils/app_theme.dart';

class AddImageWidget extends StatelessWidget {
  final void Function()? onTap;
  File? imageFile;
  final String? title;
  AddImageWidget({Key? key, this.onTap, this.imageFile, this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              height: 140,
              width: 160,
              clipBehavior: Clip.antiAliasWithSaveLayer,
              decoration: const BoxDecoration(
                  shape: BoxShape.circle, color: Colors.white),
              child: InkWell(
                onTap: onTap,
                child: imageFile != null
                    ? Image.file(
                        imageFile!,
                        //height: 60,
                        fit: BoxFit.cover,
                      )
                    : Icon(
                        Icons.add_a_photo,
                        size: 40,
                        color: Colors.grey[600],
                      ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            title!,
            style: AppTheme.bottomNavText,
          ),
        ],
      ),
    );
  }
}
