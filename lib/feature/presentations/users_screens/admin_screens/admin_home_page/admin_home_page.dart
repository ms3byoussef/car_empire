import 'package:flutter/material.dart';

import '../../../../../utils/app_theme.dart';
import 'components/counter_users_widgets/build_counter_users_widget.dart';
import 'components/custom_header.dart';

class AdminHomePageContent extends StatefulWidget {
  const AdminHomePageContent({
    Key? key,
  }) : super(key: key);

  @override
  State<AdminHomePageContent> createState() => _AdminHomePageContentState();
}

class _AdminHomePageContentState extends State<AdminHomePageContent> {
  final controller = ScrollController();
  double offset = 0;

  @override
  void initState() {
    super.initState();

    controller.addListener(onScroll);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (controller.hasClients) ? controller.offset : 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.backgroundColor,
      body: SingleChildScrollView(
        controller: controller,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            CustomHeader(
              image: "assets/images/Admin.png",
              textTop: "Admin",
              textBottom: " Panel",
              offset: offset,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
            ),
            const BuildCounterUsersWidget(),
          ],
        ),
      ),
    );
  }
}
