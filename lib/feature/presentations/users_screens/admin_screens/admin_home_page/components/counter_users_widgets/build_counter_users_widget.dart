import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';

import '../../../../../../../redux/app/app_state.dart';
import '../../../../../../../utils/app_theme.dart';
import '../../../../client_screens/client_view_model.dart';
import 'counter_widget.dart';

class BuildCounterUsersWidget extends StatelessWidget {
  const BuildCounterUsersWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      builder: (_, viewModel) => _BuildCounterUsersWidget(
        viewModel: viewModel,
      ),
      converter: (store) {
        return ClientViewModel.fromStore(store);
      },
    );
  }
}

class _BuildCounterUsersWidget extends StatefulWidget {
  final ClientViewModel viewModel;
  const _BuildCounterUsersWidget({
    Key? key,
    required this.viewModel,
  }) : super(key: key);

  @override
  State<_BuildCounterUsersWidget> createState() =>
      _BuildCounterUsersWidgetState();
}

class _BuildCounterUsersWidgetState extends State<_BuildCounterUsersWidget> {
  @override
  void initState() {
    widget.viewModel.getCars!();
    widget.viewModel.getRequests!();
    widget.viewModel.getClients!();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: "Users Update\n",
                      style: AppTheme.cardText,
                    ),
                    TextSpan(
                      text:
                          "Newest update ${DateFormat('MMMM  dd').format(DateTime.now()).toString()}",
                      style: AppTheme.primaryText,
                    ),
                  ],
                ),
              ),
              const Spacer(),
            ],
          ),
          const SizedBox(height: 20),
          Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  offset: const Offset(1, 4),
                  blurRadius: 25,
                  spreadRadius: -10,
                  color: AppTheme.primaryColor,
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                CounterWidget(
                  widget: const Icon(Icons.person_outline),
                  number: widget.viewModel.clients!.length,
                  title: "client",
                ),
                CounterWidget(
                  widget: const Icon(Icons.car_rental),
                  number: widget.viewModel.cars!.length,
                  title: "car",
                ),
                CounterWidget(
                  widget: const Icon(Icons.request_quote),
                  number: widget.viewModel.requests!.length,
                  title: "Requests",
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
