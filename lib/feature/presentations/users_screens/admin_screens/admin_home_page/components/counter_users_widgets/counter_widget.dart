import 'package:flutter/material.dart';

import '../../../../../../../utils/app_theme.dart';

class CounterWidget extends StatelessWidget {
  final int? number;
  final Widget? widget;
  final String? title;
  const CounterWidget({
    Key? key,
    this.number,
    this.widget,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(height: 10),
        Text(
          title!,
          style: AppTheme.whiteText.copyWith(
            color: const Color(0xFF959595),
          ),
        ),
        Text(
          "$number",
          style: TextStyle(
            fontSize: 40,
            color: AppTheme.accentColor,
          ),
        ),
        widget!,
      ],
    );
  }
}
