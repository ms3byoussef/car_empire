import 'package:car_empire/feature/widgets/request_card.dart';
import 'package:car_empire/feature/widgets/search_bar.dart';
import 'package:car_empire/utils/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../widgets/car_card_widget/car_card.dart';
import '../../client_screens/client_view_model.dart';

class HomeCarsScreen extends StatelessWidget {
  const HomeCarsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _CarsScreen(
        viewModel: viewModel,
      ),
    );
  }
}

class _CarsScreen extends StatefulWidget {
  final ClientViewModel viewModel;
  const _CarsScreen({Key? key, required this.viewModel}) : super(key: key);

  @override
  State<_CarsScreen> createState() => _CarsScreenState();
}

class _CarsScreenState extends State<_CarsScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  @override
  void initState() {
    widget.viewModel.getCars!();
    _tabController = TabController(length: 4, vsync: this);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    buildSellTabBar() {
      return widget.viewModel.getCarsReport?.status == ActionStatus.running
          ? const Center(child: CircularProgressIndicator())
          : widget.viewModel.cars!.isEmpty
              ? const Center(child: Text("Cars is not found for Sell"))
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: widget.viewModel.cars!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return widget.viewModel.cars![index].category == "Sell"
                        ? CarCard(car: widget.viewModel.cars![index])
                        : const SizedBox();
                  },
                );
    }

    buildRentTabBar() {
      return widget.viewModel.getCarsReport?.status == ActionStatus.running
          ? const Center(child: CircularProgressIndicator())
          : widget.viewModel.cars!.isEmpty
              ? const Center(child: Text("Cars is not found for Rent"))
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: widget.viewModel.cars!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return widget.viewModel.cars![index].category == "Rent"
                        ? CarCard(car: widget.viewModel.cars![index])
                        : const SizedBox();
                  },
                );
    }

    buildAllTabBar() {
      return widget.viewModel.getCarsReport?.status == ActionStatus.running
          ? const Center(child: CircularProgressIndicator())
          : widget.viewModel.cars!.isEmpty
              ? const Center(child: Text("Cars is not found "))
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: widget.viewModel.cars!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return CarCard(car: widget.viewModel.cars![index]);
                  },
                );
    }

    buildRequestsTabBar() {
      return widget.viewModel.getRequestsReport?.status == ActionStatus.running
          ? const Center(child: CircularProgressIndicator())
          : widget.viewModel.requests!.isEmpty
              ? const Center(child: Text("Requests is not found "))
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: widget.viewModel.requests!.length,
                  itemBuilder: (BuildContext context, int index) {
                    return RequestCard(
                        request: widget.viewModel.requests![index]);
                  },
                );
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: size.width,
              height: size.height * 1.1,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xff004c4c), Color(0xff008080)],
                  stops: [0, 1],
                ),
              ),
              child: Column(
                children: [
                  const SizedBox(
                    height: 100,
                  ),
                  Container(
                    width: size.width,
                    height: size.height - 60,
                    decoration: const BoxDecoration(
                      color: Color(0xffeff5ff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 20.0, right: 25, left: 50),
                          child: Container(
                            width: size.width,
                            height: 70,
                            decoration: BoxDecoration(
                              color: const Color(0xffffffff),
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: const [
                                BoxShadow(
                                    color: Color(0x16000000),
                                    offset: Offset(0, 2),
                                    blurRadius: 4,
                                    spreadRadius: 0)
                              ],
                            ),
                            // child: const SearchBar(),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TabBar(
                          labelStyle: AppTheme.blackText,
                          tabs: const [
                            Tab(text: "All"),
                            Tab(text: "Sells"),
                            Tab(text: "Rent "),
                            Tab(text: "Request "),
                          ],
                          labelColor: Colors.black,
                          unselectedLabelColor: Colors.grey,
                          controller: _tabController,
                          isScrollable: true,
                          indicatorColor: Colors.black,
                          padding: const EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          labelPadding:
                              const EdgeInsets.symmetric(horizontal: 20),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .6,
                          child: TabBarView(
                            controller: _tabController,
                            children: [
                              buildAllTabBar(),
                              buildSellTabBar(),
                              buildRentTabBar(),
                              buildRequestsTabBar()
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
