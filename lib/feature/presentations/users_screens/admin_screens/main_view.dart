import 'package:car_empire/feature/presentations/users_screens/client_screens/add_car_screen/add_car_screen.dart';
import 'package:car_empire/feature/presentations/users_screens/client_screens/client_view_model.dart';
import 'package:clay_containers/constants.dart';
import 'package:clay_containers/widgets/clay_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';

import '../../../../redux/app/app_state.dart';
import '../client_screens/client_home_screen/client_home_screen.dart';
import '../components/requests_screen/requests_screen.dart';
import 'home_cars_screen/home_cars_screen.dart';
import '../components/profile/profile_screen.dart';
import 'admin_home_page/admin_home_page.dart';

class MainView extends StatelessWidget {
  const MainView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      builder: (_, viewModel) => _MainView(
        viewModel: viewModel,
      ),
      converter: (store) {
        return ClientViewModel.fromStore(store);
      },
    );
  }
}

class _MainView extends StatefulWidget {
  final ClientViewModel viewModel;
  const _MainView({Key? key, required this.viewModel}) : super(key: key);

  @override
  State<_MainView> createState() => _MainViewState();
}

class _MainViewState extends State<_MainView> {
  var _currentIndex = 0;
  late List<Widget> screens;

  @override
  void initState() {
    widget.viewModel.currentUser!.role == "admin"
        ? screens = const [
            AdminHomePageContent(),
            HomeCarsScreen(),
            AddCarScreen(),
            ProfileScreen()
          ]
        : screens = const [
            ClientHomeScreen(),
            RequestsScreen(),
            AddCarScreen(),
            ProfileScreen()
          ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[_currentIndex],
      bottomNavigationBar: SizedBox(
        height: 115,
        child: Padding(
          padding:
              const EdgeInsets.only(top: 12.0, right: 20, left: 20, bottom: 20),
          child: ClayContainer(
            height: 50,
            borderRadius: 10,
            color: Colors.white,
            spread: 10,
            depth: 40,
            curveType: CurveType.convex,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: SalomonBottomBar(
                currentIndex: _currentIndex,
                onTap: (i) => setState(() => _currentIndex = i),
                items: [
                  /// Home
                  SalomonBottomBarItem(
                    icon: const Icon(Icons.home),
                    title: const Text("Home"),
                    selectedColor: Colors.teal,
                  ),

                  /// Likes
                  SalomonBottomBarItem(
                    icon: const Icon(Icons.list_alt),
                    title: const Text("List"),
                    selectedColor: Colors.teal,
                  ),

                  /// Search
                  SalomonBottomBarItem(
                    icon: const Icon(Icons.add_box_rounded),
                    title: const Text("add"),
                    selectedColor: Colors.teal,
                  ),

                  /// Profile
                  SalomonBottomBarItem(
                    icon: const Icon(Icons.person),
                    title: const Text("profile"),
                    selectedColor: Colors.teal,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
