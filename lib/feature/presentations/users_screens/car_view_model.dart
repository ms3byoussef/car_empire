import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../data/models/car_model.dart';
import '../../../data/models/user_model.dart';
import '../../../redux/action_report.dart';
import '../../../redux/app/app_state.dart';
import '../../../redux/car/car_actions.dart';

class CarViewModel {
  final ActionReport? postCarReport;
  final ActionReport? getCarsReport;
  final ActionReport? getCategoryCarsReport;
  final ActionReport? editCarReport;
  final ActionReport? removeCarReport;
  final ActionReport? searchCarsReport;

  final Function(UserModel)? setUser;
  final UserModel? user;
  final Function()? signOut;
  final Function(CarModel, File)? postCar;
  final Function()? getCars;
  final Function(String)? getCategoryCars;
  final Function(String)? getCar;
  final Function(CarModel car, {File? img})? editCar;
  final Function(String)? removeCar;
  final Function(String)? searchCarResult;
  final List<CarModel>? cars;
  final List<CarModel>? categoryCars;
  final List<CarModel>? searchCars;
  final CarModel? car;

  CarViewModel({
    this.getCategoryCarsReport,
    this.getCategoryCars,
    this.categoryCars,
    this.postCarReport,
    this.getCarsReport,
    this.editCarReport,
    this.removeCarReport,
    this.searchCarsReport,
    this.postCar,
    this.getCars,
    this.getCar,
    this.editCar,
    this.removeCar,
    this.setUser,
    this.user,
    this.cars,
    this.searchCars,
    this.searchCarResult,
    this.car,
    this.signOut,
  });

  static CarViewModel fromStore(Store<AppState>? store) {
    return CarViewModel(
      user: store!.state.authState?.user,
      car: store.state.carState!.car,
      cars: store.state.carState!.cars.values.toList(),
      categoryCars: store.state.carState!.categoryCars.values.toList(),
      searchCars: store.state.carState!.searchCar!.toList(),
      postCar: (car, img) {
        store.dispatch(PostCarAction(car: car, img: img));
      },
      getCars: () => store.dispatch(GetCarsAction()),
      getCategoryCars: (category) =>
          store.dispatch(GetCategoryCarsAction(category: category)),
      getCar: (carId) => store.dispatch(GetCarAction(carId: carId)),
      editCar: (car, {File? img}) {
        store.dispatch(EditCarAction(car: car, img: img));
      },
      removeCar: (carId) {
        store.dispatch(RemoveCarAction(carId: carId));
      },
      searchCarResult: (searchWord) {
        store.dispatch(SearchCarAction(searchWord: searchWord));
      },
      signOut: () async {
        SharedPreferences preferences;
        preferences = await SharedPreferences.getInstance();
        preferences.setBool('isLogin', false);

        await FirebaseAuth.instance.signOut();
      },
      postCarReport: store.state.carState?.status["PostCarAction"],
      getCarsReport: store.state.carState?.status["GetCarsAction"],
      getCategoryCarsReport:
          store.state.carState?.status["GetCategoryCarsAction"],
      editCarReport: store.state.carState?.status["EditCarAction"],
      removeCarReport: store.state.carState?.status["RemoveCarAction"],
      searchCarsReport: store.state.carState?.status["SearchCarAction"],
    );
  }
}
