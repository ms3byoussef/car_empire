// ignore_for_file: unused_local_variable, must_be_immutable

import 'package:cached_network_image/cached_network_image.dart';
import 'package:car_empire/feature/presentations/users_screens/admin_screens/main_view.dart';
import 'package:car_empire/utils/app_theme.dart';
import 'package:flutter/material.dart';

class Info extends StatelessWidget {
  const Info({
    Key? key,
    this.name,
    this.email,
    this.image,
  }) : super(key: key);
  final String? name, email, image;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: 280, // 240
      child: Stack(
        children: <Widget>[
          ClipPath(
            clipper: CustomShape(),
            child: Container(
              height: 200, //150
              width: double.infinity,

              color: AppTheme.primaryColor,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: IconButton(
                      onPressed: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(builder: (_) => const MainView()),
                        );
                      },
                      icon: const Icon(
                        Icons.menu,
                        color: Colors.white,
                        size: 30,
                      ),
                    )),
              ),
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(
                    bottom: 10,
                  ), //10
                  height: 140, //140
                  width: 90,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Colors.white,
                      width: 1, //8
                    ),
                  ),
                  child: CachedNetworkImage(
                    imageUrl: image!,
                    placeholder: (context, url) =>
                        Image.asset('assets/images/userImage.png'),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.error),
                  ),
                ),
                Text(
                  name!,
                  style: const TextStyle(
                    fontSize: 22, // 22
                    color: Colors.black,
                  ),
                ),
                const SizedBox(height: 5), //5
                Text(
                  email!,
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    color: Color(0xFF8492A2),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    double height = size.height;
    double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
