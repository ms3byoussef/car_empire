import 'package:car_empire/feature/auth/auth_view_model.dart';
import 'package:car_empire/feature/auth/reset_password/reset_password.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../../../../redux/app/app_state.dart';
import '../../../../../widgets/default_btn.dart';
import '../../edit_profile/edit_profile_screen.dart';
import 'profile_alert_dialog.dart';
import 'info.dart';
import 'profile_menu_item.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      distinct: true,
      converter: (store) => AuthViewModel.fromStore(store),
      builder: (_, viewModel) => _Body(
        viewModel: viewModel,
      ),
    );
  }
}

class _Body extends StatefulWidget {
  final AuthViewModel viewModel;
  const _Body({required this.viewModel});

  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Info(
            image: widget.viewModel.user!.photo,
            name: widget.viewModel.user!.name,
            email: widget.viewModel.user!.email,
          ),
          const SizedBox(height: 20), //20
          widget.viewModel.user!.role != "client"
              ? ProfileMenuItem(
                  icon: const Icon(Icons.edit_attributes),
                  title: "Edit Profile",
                  press: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (_) => const EditProfileScreen()));
                  },
                )
              : const SizedBox(),

          ProfileMenuItem(
            icon: const Icon(Icons.help),
            title: "Get Help",
            press: () {},
          ),
          ProfileMenuItem(
            icon: const Icon(Icons.add_ic_call_rounded),
            title: "Call Us",
            press: () {
              showDialog(
                  context: context, builder: (_) => const ProfileAlertDialog());
            },
          ),
          ProfileMenuItem(
            icon: const Icon(Icons.settings),
            title: "Reset Password",
            press: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => const ResetPassword(),
              ));
            },
          ),
          const SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 25),
            child: DefaultButton(
              press: () async {
                widget.viewModel.signOut!();
                Navigator.of(context).pushReplacementNamed("/");
                setState(() {});
              },
              text: "Logout",
            ),
          ),
        ],
      ),
    );
  }
}
