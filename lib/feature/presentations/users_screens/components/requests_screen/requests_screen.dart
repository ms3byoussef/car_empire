// ignore_for_file: library_private_types_in_public_api

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../widgets/request_card.dart';
import '../../admin_screens/admin_home_page/components/custom_header.dart';
import '../../client_screens/client_view_model.dart';

class RequestsScreen extends StatelessWidget {
  const RequestsScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      builder: (_, viewModel) => RequestsScreenContent(
        viewModel: viewModel,
      ),
      converter: (store) {
        return ClientViewModel.fromStore(store);
      },
    );
  }
}

class RequestsScreenContent extends StatefulWidget {
  final ClientViewModel? viewModel;

  const RequestsScreenContent({this.viewModel, Key? key}) : super(key: key);

  @override
  _RequestsScreenContentState createState() => _RequestsScreenContentState();
}

class _RequestsScreenContentState extends State<RequestsScreenContent> {
  final scrollController = ScrollController();
  double offset = 0;

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  void onScroll() {
    setState(() {
      offset = (scrollController.hasClients) ? scrollController.offset : 0;
    });
  }

  @override
  void initState() {
    widget.viewModel?.getRequests!();
    scrollController.addListener(onScroll);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomHeader(
              image: "assets/images/Admin.png",
              textTop: "Show All",
              textBottom: "Requests",
              offset: offset,
            ),
            widget.viewModel!.currentUser!.role != "admin"
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20, bottom: 20, left: 20),
                        child: Text(
                          "My Requests",
                          style: AppTheme.blackHeadline,
                        ),
                      ),
                      (widget.viewModel!.getRequestsReport?.status ==
                              ActionStatus.running)
                          ? const Center(child: CircularProgressIndicator())
                          : widget.viewModel!.requests!.isEmpty
                              ? const Center(
                                  child: Text(
                                  "Requests is not found",
                                ))
                              : SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * .4,
                                  child: ListView.builder(
                                      scrollDirection: Axis.vertical,
                                      itemCount:
                                          widget.viewModel?.requests!.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return widget.viewModel!.currentUser!
                                                    .id ==
                                                widget.viewModel
                                                    ?.requests![index].userID
                                            ? RequestCard(
                                                request: widget.viewModel
                                                    ?.requests![index])
                                            : const SizedBox();
                                      }),
                                ),
                    ],
                  )
                : const SizedBox(),
            const SizedBox(height: 20),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 20, left: 20),
                  child: Text(
                    "Requests For My Cars",
                    style: AppTheme.blackHeadline,
                  ),
                ),
                (widget.viewModel!.getRequestsReport?.status ==
                        ActionStatus.running)
                    ? const Center(child: CircularProgressIndicator())
                    : widget.viewModel!.requests!.isEmpty
                        ? const Center(
                            child: Text(
                            "Requests is not found",
                          ))
                        : SizedBox(
                            height:
                                widget.viewModel!.currentUser!.role == "admin"
                                    ? MediaQuery.of(context).size.height * .4
                                    : MediaQuery.of(context).size.height * .7,
                            child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              itemCount: widget.viewModel?.requests!.length,
                              itemBuilder: (BuildContext context, int index) {
                                return widget.viewModel!.currentUser!.id ==
                                        widget.viewModel?.requests![index].car!
                                            .ownerID!
                                    ? RequestCard(
                                        request:
                                            widget.viewModel?.requests![index])
                                    : const SizedBox();
                              },
                            ),
                          ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
