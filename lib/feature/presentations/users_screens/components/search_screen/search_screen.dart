import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:searchable_listview/searchable_listview.dart';

import '../../../../../data/models/car_model.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../widgets/car_card_widget/car_card.dart';
import '../../client_screens/client_view_model.dart';

class SearchScreen extends StatelessWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _SearchScreen(
        viewModel: viewModel,
      ),
    );
  }
}

class _SearchScreen extends StatefulWidget {
  final ClientViewModel viewModel;
  const _SearchScreen({Key? key, required this.viewModel}) : super(key: key);

  @override
  State<_SearchScreen> createState() => _SearchScreenState();
}

class _SearchScreenState extends State<_SearchScreen> {
  @override
  void initState() {
    widget.viewModel.getCars!();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: size.width,
              height: size.height * 1.1,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xff004c4c), Color(0xff008080)],
                  stops: [0, 1],
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16, vertical: 20),
                      child: IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                      )),
                  const SizedBox(
                    height: 30,
                  ),
                  Container(
                    width: size.width,
                    height: size.height - 70,
                    decoration: const BoxDecoration(
                      color: Color(0xffeff5ff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        top: 20.0,
                      ),
                      child: Container(
                        width: size.width,
                        height: 70,
                        decoration: BoxDecoration(
                          color: const Color(0xffffffff),
                          borderRadius: BorderRadius.circular(15),
                          boxShadow: const [
                            BoxShadow(
                                color: Color(0x16000000),
                                offset: Offset(0, 2),
                                blurRadius: 4,
                                spreadRadius: 0)
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: SearchableList<CarModel>(
                            initialList: widget.viewModel.cars,
                            builder: (CarModel car) => CarCard(car: car),
                            filter: (value) => widget.viewModel.cars!
                                .where(
                                  (element) => element.title!
                                      .toLowerCase()
                                      .contains(value.toLowerCase()),
                                )
                                .toList(),
                            spaceBetweenSearchAndList: 20,
                            emptyWidget: const Icon(Icons.error_outline),
                            inputDecoration: InputDecoration(
                              constraints: const BoxConstraints(
                                  minHeight: 55, maxHeight: 55),
                              labelText: "Search cars",
                              fillColor: Colors.white,
                              focusedBorder: OutlineInputBorder(
                                gapPadding: 8,
                                borderSide: const BorderSide(
                                  color: Colors.blue,
                                  width: 1.0,
                                ),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
