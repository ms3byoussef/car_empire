import 'package:car_empire/feature/presentations/users_screens/components/edit_car_screen/edit_car_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../data/models/car_model.dart';
import '../../../../../data/models/request_model.dart';
import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../widgets/car_details_card.dart';
import '../../client_screens/client_view_model.dart';

class CarDetailsScreen extends StatelessWidget {
  final CarModel car;
  const CarDetailsScreen({Key? key, required this.car}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _CarDetailsScreen(
        viewModel: viewModel,
        car: car,
      ),
    );
  }
}

class _CarDetailsScreen extends StatefulWidget {
  final CarModel? car;
  final ClientViewModel viewModel;
  const _CarDetailsScreen({Key? key, this.car, required this.viewModel})
      : super(key: key);

  @override
  State<_CarDetailsScreen> createState() => _CarDetailsScreenState();
}

class _CarDetailsScreenState extends State<_CarDetailsScreen> {
  ProgressDialog? progressDialog;

  @override
  void didUpdateWidget(_CarDetailsScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel.postRequestReport?.status == ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage(
            "Request...",
          );
          progressDialog!.show();
        }
      } else if (widget.viewModel.postRequestReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context as OverlayState,
          CustomSnackBar.error(
            message: widget.viewModel.postRequestReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel.postRequestReport?.status ==
          ActionStatus.complete) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        Navigator.pop(context);
        widget.viewModel.postRequestReport?.status = null;
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  postRequest() async {
    RequestModel request = RequestModel(
      car: widget.car,
      userID: widget.viewModel.currentUser!.id,
      status: "waiting",
      orderTime: DateFormat.yMMMEd().format(DateTime.now()).toString(),
    );
    widget.viewModel.postRequest!(request);
  }

  buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 600,
      pinned: true,
      stretch: true,
      flexibleSpace: FlexibleSpaceBar(
        title: Text(widget.car!.title!,
            style: AppTheme.whiteText.copyWith(fontSize: 22)),
        background: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(31),
            gradient: const LinearGradient(
              colors: [Color(0xff004c4c), Color(0xff008080)],
              stops: [0, 1],
              begin: Alignment(-1.00, 0.00),
              end: Alignment(1.00, -0.00),
              // angle: 90,
              // scale: undefined,
            ),
            image: const DecorationImage(
                image: AssetImage(
                  "assets/images/background_right.png",
                ),
                fit: BoxFit.contain),
          ),
          padding: const EdgeInsets.all(30),
          child: Hero(
            tag: widget.car!.id,
            child: Image.network(
              widget.car!.carImage!,
              fit: BoxFit.contain,
              width: 80,
              height: 80,
            ),
          ),
        ),
      ),
    );
  }

  priceAndBookNow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            margin: const EdgeInsets.only(left: 30),
            child: widget.car!.price != null
                ? RichText(
                    text: TextSpan(
                      style: const TextStyle(
                          fontSize: 20,
                          color: Colors.black87,
                          fontWeight: FontWeight.w500),
                      children: <TextSpan>[
                        TextSpan(text: widget.car!.price!.toString()),
                        const TextSpan(
                            text: '/KWD',
                            style: TextStyle(color: Colors.black38)),
                      ],
                    ),
                  )
                : const CircularProgressIndicator()),
        SizedBox(
            width: 200,
            height: 75,
            child: widget.car!.status != "inProgress"
                ? widget.viewModel.currentUser!.id != widget.car!.ownerID
                    ? ElevatedButton(
                        onPressed: postRequest,
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: AppTheme.primaryColor,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                            ),
                          ),
                        ),
                        child: Text(
                          "Request",
                          style: AppTheme.blackHeadline
                              .copyWith(color: Colors.white),
                        ),
                      )
                    : ElevatedButton(
                        onPressed: () async {
                          await Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (_) => EditCarScreen(
                                car: widget.car,
                              ),
                            ),
                            // (route) => false,
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor: AppTheme.primaryColor,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                            ),
                          ),
                        ),
                        child: Text(
                          "Edit Car",
                          style: AppTheme.blackHeadline
                              .copyWith(color: Colors.white),
                        ),
                      )
                : Image.asset(
                    "assets/images/unavailable.jpg",
                    width: 100,
                    height: 120,
                    fit: BoxFit.contain,
                  ))
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          buildSliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                const SizedBox(
                  height: 30,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          CarDetailsCard(
                              title: widget.car!.carDetails!.brand!,
                              subTitle: "",
                              iconsOrText: Image.asset(
                                "assets/icons/brand.png",
                                width: 40,
                              )),
                          CarDetailsCard(
                              title: widget.car!.carDetails!.modelBody!,
                              subTitle: "",
                              iconsOrText: Image.asset(
                                "assets/icons/modelBody.png",
                                width: 40,
                              )),
                          CarDetailsCard(
                              title: widget.car!.carDetails!.modelYear!,
                              subTitle: "",
                              iconsOrText: Image.asset(
                                "assets/icons/modelYear.png",
                                width: 40,
                              )),
                          CarDetailsCard(
                              title: widget.car!.carDetails!.engine!,
                              subTitle: "",
                              iconsOrText: Image.asset(
                                "assets/icons/engine.png",
                                width: 40,
                              )),
                          CarDetailsCard(
                            title: widget.car!.carDetails!.transmission!,
                            subTitle: "",
                            iconsOrText: Image.asset(
                              "assets/icons/transmission.png",
                              width: 40,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CarDetailsCard(
                            title: widget.car!.carDetails!.numberOfGears!,
                            subTitle: "",
                            iconsOrText: Image.asset(
                              "assets/icons/gear.png",
                              width: 40,
                            ),
                          ),
                          CarDetailsCard(
                            title: widget.car!.carDetails!.fuel!,
                            subTitle: "",
                            iconsOrText: Image.asset(
                              "assets/icons/fuel.png",
                              width: 40,
                            ),
                          ),
                          CarDetailsCard(
                            title: widget.car!.carDetails!.numberOfAirbag!,
                            subTitle: "",
                            iconsOrText: Image.asset(
                              "assets/icons/airbag.png",
                              width: 40,
                            ),
                          ),
                          CarDetailsCard(
                            title: widget.car!.carDetails!.numberOfSeating!,
                            subTitle: "",
                            iconsOrText: Image.asset(
                              "assets/icons/noOfSeat.png",
                              width: 40,
                            ),
                          ),
                          CarDetailsCard(
                            title: widget.car!.carDetails!.numberOfGears!,
                            subTitle: "",
                            iconsOrText: Image.asset(
                              "assets/icons/gear.png",
                              width: 40,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 40,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                  child: Text(
                    "Car Description",
                    style: AppTheme.blackHeadline,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(20),
                  margin:
                      const EdgeInsets.only(right: 20, left: 20, bottom: 30),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(0, 4),
                        blurRadius: 30,
                        color: AppTheme.primaryColor,
                      ),
                    ],
                  ),
                  child: Text(
                    widget.car!.description ?? "",
                    style: AppTheme.cardText.copyWith(color: Colors.black),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                  child: Text(
                    "this Car for : ${widget.car!.category}",
                    style: AppTheme.blackHeadline,
                  ),
                ),
                priceAndBookNow()
              ],
            ),
          )
        ],
      ),
    );
  }
}
