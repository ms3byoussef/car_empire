// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
// ignore: library_prefixes
import 'package:location/location.dart' as LocationManager;

import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../auth/auth_view_model.dart';

class MapScreen extends StatelessWidget {
  const MapScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      distinct: true,
      converter: (store) => AuthViewModel.fromStore(store),
      builder: (_, viewModel) => _MapScreen(
        viewModel: viewModel,
      ),
    );
  }
}

class _MapScreen extends StatefulWidget {
  final AuthViewModel? viewModel;
  const _MapScreen({this.viewModel});
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<_MapScreen> {
  late GoogleMapController mapController;
  LatLng? latLngg;

  List<Marker> markers = [];
  final String _address = "";
  double? lat;
  double? long;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ProgressDialog? progressDialog;
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  void didUpdateWidget(_MapScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(
      Duration.zero,
      () {
        if (widget.viewModel?.getAddressReport?.status ==
            ActionStatus.running) {
          progressDialog ??= ProgressDialog(context);
          if (!progressDialog!.isShowing()) {
            progressDialog!.setMessage("Pick Location...");
            progressDialog!.show();
          }
        } else {
          if (widget.viewModel!.getAddressReport?.status ==
              ActionStatus.error) {
            if (progressDialog != null && progressDialog!.isShowing()) {
              progressDialog!.hide();
              progressDialog = null;
            }
            showTopSnackBar(
              context as OverlayState,
              CustomSnackBar.error(
                message: widget.viewModel!.getAddressReport!.msg.toString(),
              ),
            );
            widget.viewModel!.getAddressReport?.status = null;
          } else if (widget.viewModel!.getAddressReport?.status ==
              ActionStatus.complete) {
            if (progressDialog != null && progressDialog!.isShowing()) {
              progressDialog!.hide();
              progressDialog = null;
            }
            widget.viewModel!.getAddressReport?.status = null;
            Navigator.pop(context, widget.viewModel!.address);

            setState(() {});
          } else {
            if (progressDialog != null && progressDialog!.isShowing()) {
              progressDialog!.hide();
              progressDialog = null;
            }
          }
        }
        widget.viewModel?.getAddressReport?.status = null;
      },
    );
  }

  @override
  void initState() {
    super.initState();
    markers.clear();
    latLngg = const LatLng(29.360214, 47.988245);
    lat = latLngg!.latitude;
    long = latLngg!.longitude;
  }

  final LatLng _center = const LatLng(29.360214, 47.988245);

  Future<LatLng> getUserLocation() async {
    LocationManager.LocationData currentLocation;

    final location = LocationManager.Location();

    currentLocation = await location.getLocation();

    final lat = currentLocation.latitude;

    final lng = currentLocation.longitude;

    final center = LatLng(lat!, lng!);

    return center;
  }

  _handleTap(LatLng point) {
    if (markers.isEmpty) {
      setState(() {
        markers.add(Marker(
          markerId: MarkerId(point.toString()),
          position: point,
          icon: BitmapDescriptor.defaultMarkerWithHue(
              BitmapDescriptor.hueMagenta),
        ));
        widget.viewModel!.getPlace!(point);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: GoogleMap(
        onMapCreated: _onMapCreated,
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        onTap: _handleTap,
        markers: markers.toSet(),
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 11.0,
        ),
      ),
    );
  }
}
