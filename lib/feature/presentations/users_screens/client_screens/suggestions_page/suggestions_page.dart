// ignore_for_file: unused_local_variable, must_be_immutable

import 'package:car_empire/feature/presentations/users_screens/car_view_model.dart';
import 'package:car_empire/utils/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../widgets/custom_car_card.dart';
import '../../admin_screens/main_view.dart';

class SuggestionsPage extends StatelessWidget {
  final String action;
  const SuggestionsPage({Key? key, required this.action}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CarViewModel>(
      distinct: true,
      converter: (store) => CarViewModel.fromStore(store),
      builder: (_, viewModel) => _SuggestionsPage(
        viewModel: viewModel,
        action: action,
      ),
    );
  }
}

class _SuggestionsPage extends StatefulWidget {
  final String action;
  final CarViewModel viewModel;

  const _SuggestionsPage({required this.action, required this.viewModel});

  @override
  State<_SuggestionsPage> createState() => _SuggestionsPageState();
}

class _SuggestionsPageState extends State<_SuggestionsPage> {
  @override
  void initState() {
    widget.viewModel.getCategoryCars!(widget.action);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                IconButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (_) => const MainView(),
                        ),
                      );
                    },
                    icon: const Icon(Icons.menu)),
                const SizedBox(
                  height: 30,
                ),
                Text("Our picks for you", style: AppTheme.blackHeadline),
                if (widget.viewModel.user!.userInfo!.budget ==
                    "bigger 3000") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return widget.viewModel.categoryCars![index].price! >=
                              3000
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.budget ==
                    "1500 to 3000") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return (widget.viewModel.categoryCars![index].price! <=
                                      3000 &&
                                  widget.viewModel.categoryCars![index]
                                          .price! >=
                                      1500) ||
                              (widget.viewModel.categoryCars![index].price ==
                                  3000)
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.budget ==
                    "less 1500 KWD") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return widget.viewModel.categoryCars![index].price! <=
                              1500
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.isSportive !=
                    "yes") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return widget.viewModel.categoryCars![index].carDetails!
                                  .modelBody !=
                              'Sports Cars'
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.isMarried ==
                    "yes") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return (widget.viewModel.categoryCars![index].carDetails!
                                  .modelBody !=
                              'Sports Cars')
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.likeLuxury ==
                        "yes" ||
                    widget.viewModel.user!.userInfo!.isSportive == "yes") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return (widget.viewModel.categoryCars![index].carDetails!
                                      .modelBody ==
                                  'Coupe' ||
                              widget.viewModel.categoryCars![index].carDetails!
                                      .brand ==
                                  'Rolls-Royce' ||
                              widget.viewModel.categoryCars![index].carDetails!
                                      .modelBody ==
                                  'Coupe' ||
                              widget.viewModel.categoryCars![index].price! >=
                                  15000 ||
                              widget.viewModel.categoryCars![index].carDetails!
                                      .modelBody ==
                                  'Sports Cars')
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.economic == "yes" ||
                    widget.viewModel.user!.userInfo!.isSportive == "No") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return (widget.viewModel.cars![index].price! <= 5000 ||
                              widget.viewModel.cars![index].carDetails!.brand ==
                                  'Toyota' ||
                              widget.viewModel.categoryCars![index].carDetails!
                                      .modelBody !=
                                  'Sports Cars')
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.numInFamily ==
                        "5" ||
                    widget.viewModel.user!.userInfo!.isSportive == "No") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return (widget.viewModel.categoryCars![index].carDetails!
                                      .numberOfSeating ==
                                  "5" ||
                              widget.viewModel.categoryCars![index].carDetails!
                                      .modelBody ==
                                  'Jeep')
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.numInFamily ==
                    "7") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return widget.viewModel.categoryCars![index].carDetails!
                                  .numberOfSeating ==
                              "7"
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.budget ==
                    "1500 to 3000") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return widget.viewModel.categoryCars![index].price! >=
                              2000
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else if (widget.viewModel.user!.userInfo!.budget ==
                    "less 1500 KWD") ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return widget.viewModel.categoryCars![index].price! <=
                              1500
                          ? CustomCarCard(
                              car: widget.viewModel.categoryCars![index])
                          : const SizedBox();
                    },
                  ),
                ] else ...[
                  CarListInCondition(
                    itemBuilder: (BuildContext context, int index) {
                      return CustomCarCard(
                          car: widget.viewModel.categoryCars![index]);
                    },
                  ),
                ]
              ]),
        ),
      ),
    );
  }
}

class CarListInCondition extends StatelessWidget {
  Widget Function(BuildContext, int) itemBuilder;
  CarListInCondition({Key? key, required this.itemBuilder}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CarViewModel>(
      distinct: true,
      converter: (store) => CarViewModel.fromStore(store),
      builder: (_, viewModel) => _CarListInCondition(
        viewModel: viewModel,
        itemBuilder: itemBuilder,
      ),
    );
  }
}

class _CarListInCondition extends StatelessWidget {
  final CarViewModel viewModel;

  Widget Function(BuildContext, int) itemBuilder;
  _CarListInCondition({required this.itemBuilder, required this.viewModel});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return SizedBox(
      height: size.height * .7,
      child: viewModel.getCategoryCarsReport?.status == ActionStatus.running
          ? const Center(child: CircularProgressIndicator())
          : viewModel.categoryCars!.isEmpty
              ? const Center(child: Text("cars not found"))
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: viewModel.categoryCars!.length,
                  itemBuilder: itemBuilder,
                ),
    );
  }
}
