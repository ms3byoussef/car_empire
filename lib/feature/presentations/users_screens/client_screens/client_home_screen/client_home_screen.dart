import 'package:car_empire/feature/presentations/users_screens/client_screens/add_car_screen/add_car_screen.dart';
import 'package:car_empire/feature/widgets/custom_car_card.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../../utils/constant.dart';
import '../client_view_model.dart';
import '../questions_screen/questions_screen.dart';

class ClientHomeScreen extends StatelessWidget {
  const ClientHomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _ClientHomeScreen(
        viewModel: viewModel,
      ),
    );
  }
}

class _ClientHomeScreen extends StatefulWidget {
  final ClientViewModel viewModel;
  const _ClientHomeScreen({Key? key, required this.viewModel})
      : super(key: key);

  @override
  State<_ClientHomeScreen> createState() => _ClientHomeScreenState();
}

class _ClientHomeScreenState extends State<_ClientHomeScreen> {
  @override
  void initState() {
    widget.viewModel.getCars!();
    // widget.viewModel.getCompanies!();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: size.height * 1.10,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xff004c4c), Color(0xff008080)],
                  stops: [0, 1],
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 24),
                    child: Text(
                      'Hi  ${widget.viewModel.currentUser!.name}',
                      style:
                          AppTheme.blackHeadline.copyWith(color: Colors.white),
                    ),
                  ),
                  Container(
                    height: size.height - 25,
                    decoration: const BoxDecoration(
                      color: Color(0xffeff5ff),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          child: Text(
                            textAlign: TextAlign.left,
                            "What do yo need ?",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        ),
                        CarouselSlider(
                          items: [
                            SlideContent(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => const AddCarScreen()));
                              },
                              btnText: "Sell",
                              img: images[0],
                            ),
                            SlideContent(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => const QuestionsScreen(
                                              action: sell,
                                            )));
                              },
                              btnText: buy,
                              img: images[1],
                            ),
                            SlideContent(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => const QuestionsScreen(
                                              action: rent,
                                            )));
                              },
                              btnText: rent,
                              img: images[2],
                            ),
                          ],
                          options: CarouselOptions(
                              autoPlay: true,
                              aspectRatio: 1.5,
                              enlargeCenterPage: true,
                              enlargeStrategy:
                                  CenterPageEnlargeStrategy.height),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Flexible(
                          child: widget.viewModel.getCarsReport?.status ==
                                  ActionStatus.running
                              ? const Center(child: CircularProgressIndicator())
                              : widget.viewModel.cars!.isEmpty
                                  ? const Center(child: Text("cars not found"))
                                  : ListView.builder(
                                      scrollDirection: Axis.vertical,
                                      itemCount: widget.viewModel.cars!.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return CustomCarCard(
                                            car: widget.viewModel.cars![index]);
                                      },
                                    ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SlideContent extends StatelessWidget {
  final String img;
  final String btnText;
  final void Function() onTap;
  const SlideContent(
      {super.key,
      required this.img,
      required this.btnText,
      required this.onTap});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 10),
          width: size.width * .68,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              image: AssetImage(img),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(.4), BlendMode.darken),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 10,
          child: ClipPath(
            clipper: ProsteBezierCurve(
              position: ClipPosition.top,
              list: [
                BezierCurveSection(
                  start: const Offset(300, 10),
                  top: const Offset(300 / 2, 30),
                  end: const Offset(0, 0),
                ),
              ],
            ),
            child: InkWell(
              onTap: onTap,
              child: Container(
                padding: const EdgeInsets.only(top: 10),
                height: 100,
                width: size.width * .68,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Color(0xff004c4c), Color(0xff008080)],
                    stops: [0, 1],
                    begin: Alignment(-1.00, 0.00),
                    end: Alignment(1.00, -0.00),
                  ),
                ),
                child: Center(
                  child: Text(
                    btnText,
                    style: AppTheme.whiteText.copyWith(fontSize: 22),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
