import 'package:flutter/material.dart';

import '../../../../../utils/app_theme.dart';

class QuestionContent extends StatelessWidget {
  final String? imgName;
  final String? headlineText;
  final Widget? child;
  const QuestionContent({Key? key, this.imgName, this.headlineText, this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Image.asset(
          "assets/images/$imgName",
          fit: BoxFit.cover,
          height: MediaQuery.of(context).size.height * .4,
          width: MediaQuery.of(context).size.width,
        ),
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            headlineText!,
            textAlign: TextAlign.center,
            style: AppTheme.blackText.copyWith(fontSize: 20),
            maxLines: 2,
          ),
        ),
        const SizedBox(height: 10),
        Padding(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 25),
            child: child ?? const SizedBox()),
      ],
    );
  }
}
