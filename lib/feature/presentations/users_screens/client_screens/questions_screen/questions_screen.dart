import 'package:car_empire/data/models/user_info.dart';
import 'package:car_empire/feature/presentations/users_screens/client_screens/client_view_model.dart';
import 'package:car_empire/feature/presentations/users_screens/client_screens/questions_screen/question_content.dart';
import 'package:car_empire/utils/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../utils/constant.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../widgets/custom_drop_down.dart';
import '../../../../widgets/default_btn.dart';
import '../suggestions_page/suggestions_page.dart';

class QuestionsScreen extends StatelessWidget {
  final String action;
  const QuestionsScreen({Key? key, required this.action}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ClientViewModel>(
      distinct: true,
      converter: (store) => ClientViewModel.fromStore(store),
      builder: (_, viewModel) => _QuestionsScreen(
        viewModel: viewModel,
        action: action,
      ),
    );
  }
}

class _QuestionsScreen extends StatefulWidget {
  final ClientViewModel viewModel;
  final String action;

  const _QuestionsScreen(
      {Key? key, required this.viewModel, required this.action})
      : super(key: key);

  @override
  State<_QuestionsScreen> createState() => _QuestionsScreenState();
}

class _QuestionsScreenState extends State<_QuestionsScreen> {
  ProgressDialog? progressDialog;

  PageController _pageController = PageController();
  bool _lastPage = false;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(_QuestionsScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel.updateUserInfoReport?.status ==
          ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage(
            "Submitted ...",
          );
          progressDialog!.show();
        }
      } else if (widget.viewModel.updateUserInfoReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context as OverlayState,
          CustomSnackBar.error(
            message: widget.viewModel.updateUserInfoReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel.updateUserInfoReport?.status ==
          ActionStatus.complete) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => SuggestionsPage(
                action: widget.action,
              ),
            ));
        widget.viewModel.updateUserInfoReport?.status = null;
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  updateUerInfo() async {
    UserInformation userInfo = UserInformation(
      isMarried: isMarried.text,
      numInFamily: numInFamily.text,
      isSportive: sportive.text,
      budget: budget.text,
      economic: economic.text,
      likeLuxury: likeLuxury.text,
    );
    await widget.viewModel.updateUserInfo!(userInfo);
  }

  TextEditingController isMarried = TextEditingController();
  TextEditingController numInFamily = TextEditingController();
  TextEditingController sportive = TextEditingController();
  TextEditingController budget = TextEditingController();
  TextEditingController economic = TextEditingController();
  TextEditingController likeLuxury = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: AppTheme.backgroundColor,
        body: SafeArea(
          child: SingleChildScrollView(
            child: Container(
              height: size.height - 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(31),
                color: AppTheme.backgroundColor,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: size.height * .7,
                    child: PageView(
                      controller: _pageController,
                      onPageChanged: ((value) => setState(() {
                            _lastPage = (value == 5);
                          })),
                      children: [
                        QuestionContent(
                          headlineText: "Are you married?",
                          imgName: 'isMarried.png',
                          child: CustomDropDown(
                            label: "",
                            controller: isMarried,
                            hintText: "Chose Answer",
                            dropDownList: yesOrNo,
                          ),
                        ),
                        QuestionContent(
                          headlineText:
                              "How many members are there in your family?",
                          imgName: 'numInFamily.png',
                          child: CustomDropDown(
                            label: "",
                            controller: numInFamily,
                            hintText: "Chose Answer",
                            dropDownList: nums,
                          ),
                        ),
                        QuestionContent(
                          headlineText: "Do you like sports؟",
                          imgName: 'sportive.png',
                          child: CustomDropDown(
                            label: "",
                            controller: sportive,
                            hintText: "Chose Answer",
                            dropDownList: yesOrNo,
                          ),
                        ),
                        QuestionContent(
                          headlineText: "What is your budget ?",
                          imgName: 'budget.png',
                          child: CustomDropDown(
                            label: "",
                            controller: budget,
                            hintText: "Chose Answer",
                            dropDownList: budgetType,
                          ),
                        ),
                        QuestionContent(
                          headlineText: "Do you like economic car ?",
                          imgName: 'economic.png',
                          child: CustomDropDown(
                            label: "",
                            controller: economic,
                            hintText: "Chose Answer",
                            dropDownList: yesOrNo,
                          ),
                        ),
                        QuestionContent(
                          headlineText: "Do you like luxury ?",
                          imgName: 'likeLuxury.png',
                          child: CustomDropDown(
                            label: "",
                            controller: likeLuxury,
                            hintText: "Chose Answer",
                            dropDownList: yesOrNo,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: size.height * .05,
                  ),
                  SizedBox(
                    height: size.height * .05,
                    child: SmoothPageIndicator(
                      effect: SwapEffect(
                          dotColor: AppTheme.accentColor,
                          dotWidth: 20,
                          activeDotColor: AppTheme.primaryColor),
                      controller: _pageController,
                      count: 6,
                    ),
                  ),
                  SizedBox(height: size.height * .05),
                ],
              ),
            ),
          ),
        ),
        bottomSheet: _lastPage
            ? ClipPath(
                clipper: ProsteBezierCurve(
                  position: ClipPosition.top,
                  list: [
                    BezierCurveSection(
                      start: Offset(size.width, 10),
                      top: Offset(size.width / 3, 40),
                      end: const Offset(0, 0),
                    ),
                  ],
                ),
                child: InkWell(
                  onTap: updateUerInfo,
                  child: Container(
                    padding: const EdgeInsets.only(top: 10),
                    height: size.height * .15,
                    width: size.width,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: [Color(0xff004c4c), Color(0xff008080)],
                        stops: [0, 1],
                        begin: Alignment(-1.00, 0.00),
                        end: Alignment(1.00, -0.00),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        "Submit",
                        style: AppTheme.whiteText.copyWith(fontSize: 22),
                      ),
                    ),
                  ),
                ),
              )
            : DefaultButton(
                text: "Next",
                press: () {
                  _pageController.nextPage(
                      duration: const Duration(milliseconds: 100),
                      curve: Curves.easeOut);
                },
              ));
  }
}
