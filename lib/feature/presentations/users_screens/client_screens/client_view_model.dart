import 'package:car_empire/data/models/user_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../data/models/car_model.dart';
import '../../../../data/models/request_model.dart';
import '../../../../data/models/user_model.dart';
import '../../../../redux/action_report.dart';
import '../../../../redux/app/app_state.dart';
import '../../../../redux/client/client_actions.dart';
import '../../../../redux/car/car_actions.dart';

class ClientViewModel {
  final ActionReport? getCarReport;
  final ActionReport? getClientsReport;
  final ActionReport? getCarsReport;
  final ActionReport? postRequestReport;
  final ActionReport? removeRequestReport;
  final ActionReport? getRequestsReport;
  final ActionReport? updateRequestReport;
  final ActionReport? postReviewReport;
  final ActionReport? removeReviewReport;
  final ActionReport? getReviewsReport;
  final ActionReport? getRequestUserReport;
  final ActionReport? getCarOwnerUserReport;
  final ActionReport? updateUserInfoReport;
  final ActionReport? updateCarStatusReport;

  final UserModel? currentUser;

  final Function()? signOut;
  final Function({String carId, String carStatus})? updateCarStatus;

  final List<CarModel>? cars;
  final CarModel? car;
  // final Product? orderProduct;
  final Function()? getCars;
  final Function()? getClients;
  final List<UserModel>? clients;
  // final UserModel? company;

  final Function(String)? getProduct;
  final Function(UserInformation)? updateUserInfo;

  final Function(RequestModel)? postRequest;
  final Function(RequestModel)? getRequestUser;
  final Function(CarModel)? getCarOwnerUser;
  final Function(RequestModel)? getCarInRequest;
  final Function(RequestModel)? removeRequest;
  final Function()? getRequests;
  final Function({String? requestID, String? status})? updateRequest;
  final UserModel? requestUser;
  final UserModel? carOwnerUser;

  final RequestModel? request;
  final List<RequestModel>? requests;

  ClientViewModel({
    this.getCarReport,
    this.getCarsReport,
    this.getClientsReport,
    this.updateUserInfoReport,
    this.updateCarStatusReport,
    this.getClients,
    this.updateCarStatus,
    this.clients,
    this.updateUserInfo,
    this.postRequestReport,
    this.removeRequestReport,
    this.getRequestsReport,
    this.updateRequestReport,
    this.postReviewReport,
    this.removeReviewReport,
    this.getReviewsReport,
    this.getRequestUserReport,
    this.getCarOwnerUserReport,
    this.getCarOwnerUser,
    this.carOwnerUser,
    this.currentUser,
    this.signOut,
    this.cars,
    this.car,
    this.requestUser,
    // this.companies,
    // this.company,
    this.getCars,
    // this.getCompanies,
    this.getProduct,
    this.postRequest,
    this.getRequestUser,
    this.getCarInRequest,
    this.removeRequest,
    this.getRequests,
    this.updateRequest,
    this.request,
    this.requests,
  });
  static ClientViewModel fromStore(Store<AppState>? store) {
    return ClientViewModel(
      currentUser: store!.state.authState?.user,
      requestUser: store.state.clientState?.requestUser,
      carOwnerUser: store.state.clientState?.carOwnerUser,
      // reviewUser: store.state.orderState?.reviewUser,
      cars: store.state.carState?.cars.values.toList(),
      car: store.state.carState?.car,
      clients: store.state.clientState!.clients!.values.toList(),
      // company: store.state.authState!.company,
      getCars: () => store.dispatch(GetCarsAction()),
      // getCompanies: () => store.dispatch(GetCompaniesAction()),
      getProduct: (carId) {
        store.dispatch(GetCarAction(carId: carId));
      },
      // orderProduct: store.state.orderState?.orderProduct,
      request: store.state.clientState?.request,
      requests: store.state.clientState?.requests.values.toList(),
      postRequest: (request) {
        store.dispatch(PostRequestAction(request: request));
      },
      getRequestUser: (request) {
        store.dispatch(GetRequestUserAction(request: request));
      },
      getCarOwnerUser: (car) {
        store.dispatch(GetCarOwnerCompanyAction(car: car));
      },

      getClients: () {
        store.dispatch(GetClientsAction());
      },
      removeRequest: (request) {
        store.dispatch(RemoveRequestAction(request: request));
      },
      updateCarStatus: ({String? carId, String? carStatus}) {
        store.dispatch(UpdateCarStatusAction(carId: carId, status: carStatus));
      },
      getRequests: () {
        store.dispatch(GetRequestsAction());
      },
      updateRequest: ({String? requestID, String? status}) {
        store.dispatch(
            UpdateRequestAction(requestID: requestID, status: status));
      },
      updateUserInfo: (userInfo) {
        store.dispatch(UpdateUserInfoAction(userInformation: userInfo));
      },
      signOut: () async {
        SharedPreferences preferences = await SharedPreferences.getInstance();
        // ignore: unused_local_variable
        bool? isLogin = preferences.getBool("isLogin") ?? false;
        preferences.setBool("isLogin", false);
        // preferences.remove('users');
        // preferences.clear();
        await FirebaseAuth.instance.signOut();
      },

      getCarReport: store.state.carState?.status['GetCarAction'],
      getCarsReport: store.state.carState?.status['GetCarsAction'],
      getClientsReport: store.state.authState!.status!['GetClientsAction'],
      postRequestReport: store.state.clientState?.status['PostRequestAction'],
      removeRequestReport:
          store.state.clientState?.status['RemoveRequestAction'],
      getRequestsReport: store.state.clientState?.status['GetRequestsAction'],
      updateRequestReport:
          store.state.clientState?.status['UpdateRequestAction'],

      postReviewReport: store.state.clientState?.status['PostReviewAction'],
      removeReviewReport: store.state.clientState?.status['RemoveReviewAction'],
      getReviewsReport: store.state.clientState?.status['GetReviewsAction'],
      getRequestUserReport:
          store.state.clientState?.status['GetRequestUserAction'],
      getCarOwnerUserReport:
          store.state.clientState?.status['GetCarOwnerCompanyAction'],
      updateUserInfoReport:
          store.state.clientState?.status['UpdateUserInfoAction'],
      updateCarStatusReport:
          store.state.clientState?.status['UpdateCarStatusAction'],
    );
  }
}
