// ignore_for_file: empty_catches, library_private_types_in_public_api, unused_local_variable

import 'dart:io';

import 'package:car_empire/feature/widgets/custom_drop_down.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_select_items/flutter_multi_select_items.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:image_picker/image_picker.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../data/models/car_details_model.dart';
import '../../../../../data/models/car_model.dart';
import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../utils/app_theme.dart';
import '../../../../../utils/constant.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../../utils/validator.dart';
import '../../../../widgets/add_image_widgets.dart';
import '../../../../widgets/custom_form_field.dart';
import '../../../../widgets/default_btn.dart';
import '../../../../widgets/search_drop_down.dart';
import '../../admin_screens/main_view.dart';
import '../../car_view_model.dart';

class AddCarScreen extends StatelessWidget {
  const AddCarScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CarViewModel>(
      builder: (_, viewModel) => _AddCarScreenContent(
        viewModel: viewModel,
      ),
      converter: (store) {
        return CarViewModel.fromStore(store);
      },
    );
  }
}

class _AddCarScreenContent extends StatefulWidget {
  final CarViewModel? viewModel;

  const _AddCarScreenContent({Key? key, this.viewModel}) : super(key: key);

  @override
  _AddCarScreenState createState() => _AddCarScreenState();
}

class _AddCarScreenState extends State<_AddCarScreenContent> {
  TextEditingController title = TextEditingController();
  TextEditingController price = TextEditingController();
  TextEditingController category = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController brand = TextEditingController();
  TextEditingController modelYear = TextEditingController();
  TextEditingController modelBody = TextEditingController();
  TextEditingController fuel = TextEditingController();
  TextEditingController engine = TextEditingController();
  TextEditingController transmission = TextEditingController();
  TextEditingController numberOfAirbag = TextEditingController();
  TextEditingController numberOfGears = TextEditingController();
  TextEditingController numberOfSeating = TextEditingController();

  ProgressDialog? progressDialog;
  final ImagePicker _picker = ImagePicker();
  File? carImage;
  List<int> _carColors = [];

  Future pickImage() async {
    try {
      final image = await _picker.pickImage(source: ImageSource.gallery);
      if (image == null) return; // Capture a photo

      setState(() => carImage = File(image.path));
      // ignore: unused_catch_clause
    } on PlatformException catch (e) {}
  }

  addCar() async {
    if (title.text.isEmpty) {
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: " Car title is empty",
        ),
      );
      return;
    }

    if (price.text.isEmpty) {
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: " Car price is empty",
        ),
      );
      return;
    }
    CarDetailsModel carDetails = CarDetailsModel(
        brand: brand.text,
        engine: engine.text,
        fuel: fuel.text,
        modelBody: modelBody.text,
        modelYear: modelYear.text,
        transmission: transmission.text,
        numberOfAirbag: numberOfAirbag.text,
        numberOfGears: numberOfGears.text,
        numberOfSeating: numberOfSeating.text,
        colors: _carColors);
    int? carPrice = int.tryParse(price.text);

    CarModel car = CarModel(
        title: title.text,
        price: carPrice,
        description: description.text,
        ownerID: widget.viewModel!.user!.id,
        category: category.text,
        carDetails: carDetails);

    widget.viewModel!.postCar!(car, carImage!);
  }

  @override
  void didUpdateWidget(_AddCarScreenContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel!.postCarReport?.status == ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage(
            "Adding Car ...",
          );
          progressDialog!.show();
        }
      } else if (widget.viewModel!.postCarReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context as OverlayState,
          CustomSnackBar.error(
            message: widget.viewModel!.postCarReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel!.postCarReport?.status ==
          ActionStatus.complete) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => const MainView(),
        ));
        widget.viewModel!.postCarReport?.status = null;
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          buildSliverAppBar(),
          SliverList(
            delegate: SliverChildListDelegate(
              [buildCarDetailsForm()],
            ),
          )
        ],
      ),
    );
  }

  buildCarDetailsForm() {
    final size = MediaQuery.of(context).size;

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      color: const Color(0x1a009444).withOpacity(.3),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: size.height * .04,
          ),
          Container(
            padding: const EdgeInsets.only(
              bottom:
                  4, // This can be the space you need between text and underline
            ),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: AppTheme.primaryColor,
                  width: 2, // This would be the width of the underline
                ),
              ),
            ),
            child: Text("Add Car title & price", style: AppTheme.blackHeadline),
          ),
          const SizedBox(height: 20),
          Row(
            children: [
              Expanded(
                child: CustomFormField(
                  controller: title,
                  label: "Car title ",
                  hintText: "Enter Car title ",
                  keyboardType: TextInputType.name,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomFormField(
                  controller: price,
                  label: "price",
                  hintText: "Enter Car price ",
                  keyboardType: TextInputType.number,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
            ],
          ),
          CustomFormField(
            maxLine: true,
            controller: description,
            label: "description",
            labelTextStyle: AppTheme.hintText.copyWith(color: Colors.black),
            hintText: "Enter Car description ",
            keyboardType: TextInputType.multiline,
            validator: (value) => Validator.validateEmpty(value!),
          ),
          CustomDropDown(
            controller: category,
            label: "Category ",
            hintText: "Enter Category ",
            labelTextStyle: AppTheme.hintText.copyWith(color: Colors.black),
            dropDownList: widget.viewModel!.user!.role == "client"
                ? carCategoryForClient
                : carCategoryForAdmin,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18),
            child: Text(" Car Details ", style: AppTheme.blackHeadline),
          ),
          Row(
            children: [
              Expanded(
                child: CustomSearchDropDown(
                  label: "brand ",
                  controller: brand,
                  hintText: "Enter Car brand ",
                  dropDownList: carBrands,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomFormField(
                  controller: modelYear,
                  label: "modelYear ",
                  hintText: "Enter Car modelYear ",
                  keyboardType: TextInputType.number,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: CustomSearchDropDown(
                  controller: modelBody,
                  label: "modelBody ",
                  dropDownList: carBody,
                  hintText: "Enter modelBody ",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomDropDown(
                  controller: fuel,
                  label: "fuel ",
                  hintText: "Enter fuel ",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  dropDownList: fuelType,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: CustomFormField(
                  controller: engine,
                  label: "engine ",
                  hintText: "Enter engine ",
                  keyboardType: TextInputType.name,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomDropDown(
                  controller: transmission,
                  label: "transmission ",
                  hintText: "Enter transmission",
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  dropDownList: transmissionsTypes,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: CustomFormField(
                  controller: numberOfAirbag,
                  label: "numberOfAirbag ",
                  hintText: "Ex 5 ",
                  keyboardType: TextInputType.name,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: CustomFormField(
                  controller: numberOfGears,
                  label: "numberOfGears ",
                  hintText: "Ex 3 ",
                  keyboardType: TextInputType.number,
                  labelTextStyle:
                      AppTheme.hintText.copyWith(color: Colors.black),
                  validator: (value) => Validator.validateEmpty(value!),
                ),
              ),
            ],
          ),
          CustomFormField(
            controller: numberOfSeating,
            label: "numberOfSeating ",
            hintText: "ex 4",
            labelTextStyle: AppTheme.hintText.copyWith(color: Colors.black),
            keyboardType: TextInputType.number,
            validator: (value) => Validator.validateEmpty(value!),
          ),
          const SizedBox(height: 20),
          Text("Colors of Car :", style: AppTheme.blackText),
          MultiSelectContainer(
              splashColor: Colors.black,
              textStyles: const MultiSelectTextStyles(
                  textStyle: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black)),
              items: carColors
                  .map((e) => MultiSelectCard(
                      decorations: MultiSelectItemDecorations(
                        decoration: BoxDecoration(
                          color: Colors.purple.withOpacity(0.2),
                        ),
                        selectedDecoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(width: 3, color: Colors.black)),
                      ),
                      value: e.value,
                      child: CircleAvatar(
                        radius: 14,
                        backgroundColor: e,
                      )))
                  .toList(),
              onChange: (allSelectedItems, selectedItem) {
                _carColors = allSelectedItems;
              }),
          const SizedBox(height: 20),
          DefaultButton(
            text: "Add Car",
            press: () async {
              addCar();
            },
          ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }

  buildSliverAppBar() {
    return SliverAppBar(
      expandedHeight: 300,
      pinned: false,
      stretch: true,
      flexibleSpace: FlexibleSpaceBar(
        title:
            Text("Add Car", style: AppTheme.whiteText.copyWith(fontSize: 22)),
        background: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30),
            ),
            gradient: LinearGradient(
              colors: [Color(0xff004c4c), Color(0xff008080)],
              stops: [0, 1],
              begin: Alignment(-1.00, 0.00),
              end: Alignment(1.00, -0.00),
              // angle: 90,
              // scale: undefined,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 40),
            child: AddImageWidget(
              imageFile: carImage,
              onTap: pickImage,
              title: "Add Car Image",
            ),
          ),
        ),
      ),
    );
  }
}
