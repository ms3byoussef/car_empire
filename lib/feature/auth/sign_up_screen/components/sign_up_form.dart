// ignore_for_file: library_private_types_in_public_api, avoid_print

import 'package:car_empire/data/models/user_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../data/models/user_model.dart';
import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../../utils/validator.dart';

import '../../../../utils/app_theme.dart';
import '../../../presentations/users_screens/components/map_screen/map_screen.dart';
import '../../../presentations/users_screens/admin_screens/main_view.dart';
import '../../../widgets/custom_form_field.dart';
import '../../../widgets/custom_text_button.dart';
import '../../../widgets/password_form_field.dart';
import '../../auth_view_model.dart';

class SignUpForm extends StatelessWidget {
  const SignUpForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      builder: (_, viewModel) => SignUpFormContent(
        viewModel: viewModel,
      ),
      converter: (store) {
        return AuthViewModel.fromStore(store);
      },
    );
  }
}

class SignUpFormContent extends StatefulWidget {
  final AuthViewModel? viewModel;

  const SignUpFormContent({
    Key? key,
    this.viewModel,
  }) : super(key: key);
  @override
  _SignUpFormContentState createState() => _SignUpFormContentState();
}

class _SignUpFormContentState extends State<SignUpFormContent> {
  final _formKey = GlobalKey<FormState>();

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController confirmPassword = TextEditingController();
  ProgressDialog? progressDialog;

  signUpComplete() async {
    final UserInformation userInfo = UserInformation();
    final UserModel user = UserModel(
        name: name.text,
        email: email.text,
        phone: phone.text,
        address: address.text,
        userInfo: userInfo);

    await widget.viewModel!.signUp!(user, password.text);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(SignUpFormContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(
      Duration.zero,
      () {
        if (widget.viewModel!.getSignUpReport?.status == ActionStatus.running) {
          progressDialog ??= ProgressDialog(context);
          if (!progressDialog!.isShowing()) {
            progressDialog!.setMessage("Sign Up ...");
            progressDialog!.show();
          }
        } else if (widget.viewModel!.getSignUpReport?.status ==
            ActionStatus.error) {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
          showTopSnackBar(
            context as OverlayState,
            CustomSnackBar.error(
              message: widget.viewModel!.getSignUpReport!.msg.toString(),
            ),
          );
          widget.viewModel!.getSignUpReport?.status = null;
        } else if (widget.viewModel!.getSignUpReport?.status ==
            ActionStatus.complete) {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (_) => const MainView()),
              (route) => false);
          widget.viewModel!.getSignUpReport?.status = null;
        } else {
          if (progressDialog != null && progressDialog!.isShowing()) {
            progressDialog!.hide();
            progressDialog = null;
          }
        }
        widget.viewModel!.getSignUpReport?.status = null;
      },
    );
  }

  Widget _buildNameTF() {
    return CustomFormField(
      label: "Name",
      controller: name,
      hintText: "Enter your name",
      keyboardType: TextInputType.name,
      validator: (value) => Validator.validateEmpty(value!),
      prefixIcon: const Icon(
        Icons.person,
        color: Colors.white,
      ),
    );
  }

  Widget _buildEmailTF() {
    return CustomFormField(
      label: "Email",
      controller: email,
      hintText: "Enter your email",
      keyboardType: TextInputType.emailAddress,
      validator: (value) => Validator.validateEmail(value!),
      prefixIcon: const Icon(
        Icons.email,
        color: Colors.white,
      ),
    );
  }

  Widget _buildPhoneTF() {
    return CustomFormField(
      label: "Phone",
      controller: phone,
      hintText: "Enter your phone",
      keyboardType: TextInputType.phone,
      validator: (value) => Validator.validateEmpty(value!),
      prefixIcon: const Icon(
        Icons.phone,
        color: Colors.white,
      ),
    );
  }

  Widget _buildAddressTF() {
    return CustomFormField(
      label: "address",
      suffixIcon: IconButton(
        onPressed: () async {
          final result = await Navigator.of(context).push(
            MaterialPageRoute(
                builder: (BuildContext context) => const MapScreen()),
          );
          if (result != null) {
            print("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr$result");
            setState(() {
              address.text = result;
            });
          }
        },
        icon: const Icon(
          Icons.location_on_sharp,
          color: Colors.white,
        ),
      ),
      controller: address,
      prefixIcon: const SizedBox(width: 4),
      hintText: "ex home or work address",
      validator: (value) => Validator.validateEmpty(value!),
      keyboardType: TextInputType.streetAddress,
    );
  }

  Widget _buildPasswordTF() {
    return PasswordFormField(
      label: "password",
      controller: password,
      hintText: "ex *********",
      validator: (value) => Validator.validatePassword(value!),
      prefixIcon: const Icon(
        Icons.lock,
        color: Colors.white,
      ),
    );
  }

  // Widget _buildConfirmPasswordTF() {
  //   return PasswordFormField(
  //     label: 'confirmPassword',
  //     controller: confirmPassword,
  //     hintText: "confirm your password",
  //     validator: (value) {
  //       if (value!.isEmpty) return "Empty";
  //       if (value != password.text) {
  //         return "Not match";
  //       }
  //       return null;
  //     },
  //     prefixIcon: const Icon(
  //       Icons.lock,
  //       color: Colors.white,
  //     ),
  //   );
  // }

  void signUp() async {
    if (email.text.isEmpty) {
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: "Email is empty",
        ),
      );
      return;
    }
    if (password.text.isEmpty) {
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: "password is empty",
        ),
      );
      return;
    }
    if (address.text.isEmpty) {
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: "address is empty",
        ),
      );
      return;
    }
    if (password.text.length < 5) {
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: "password is less than 6 ",
        ),
      );
      return;
    }
    // if (confirmPassword.text.isEmpty) {
    //   showTopSnackBar(
    //     context,
    //     const CustomSnackBar.error(
    //       message: "ConfirmPassword is empty",
    //     ),
    //   );
    //   return;
    // }
    // if (confirmPassword.text != password.text) {
    //   showTopSnackBar(
    //     context,
    //     const CustomSnackBar.error(
    //       message: "confirmPassword and password don't match",
    //     ),
    //   );
    //   return;
    // }
    if (_formKey.currentState!.validate()) {
      signUpComplete();
    } else {
      print("Not Valid");
    }
  }

  Widget _buildLoginBtn() {
    return Padding(
      padding: const EdgeInsets.only(top: 30),
      child: CustomTextButton(onPressed: signUp, text: "Sign Up"),
    );
  }

  Widget _buildSignInWithText() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: Column(
        children: [
          Text(
            "__OR__",
            style: AppTheme.whiteText.copyWith(fontSize: 18),
          ),
          const SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "have an account ",
                style: AppTheme.whiteText
                    .copyWith(fontSize: 18, color: AppTheme.backgroundColor),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/login");
                },
                child: Text(
                  ' Sign in',
                  style: AppTheme.whiteText.copyWith(fontSize: 21),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              _buildNameTF(),
              _buildEmailTF(),
              _buildPhoneTF(),
              _buildAddressTF(),
              _buildPasswordTF(),
              _buildLoginBtn(),
              _buildSignInWithText(),
            ],
          ),
        ));
  }
}
