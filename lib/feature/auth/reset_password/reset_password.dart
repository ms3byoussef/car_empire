import 'package:car_empire/feature/widgets/custom_form_field.dart';
import 'package:car_empire/feature/widgets/default_btn.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../utils/validator.dart';
import '../../../utils/app_theme.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({Key? key}) : super(key: key);

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  TextEditingController email = TextEditingController();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  reset() async {
    try {
      await firebaseAuth.sendPasswordResetEmail(email: email.text);

      // ignore: use_build_context_synchronously
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.success(
          message:
              // ignore: use_build_context_synchronously
              "success, please check your email.",
        ),
      );
      // ignore: use_build_context_synchronously
      Navigator.of(context).pop();
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: AppTheme.backgroundColor,
      body: SingleChildScrollView(
        child: Container(
          height: size.height * 1.4,
          width: size.width,
          color: AppTheme.primaryColor,
          child: Column(
            children: [
              Container(
                margin: const EdgeInsets.only(
                  top: 20,
                ),
                height: size.height * .37,
                width: size.width,
                color: AppTheme.primaryColor,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(),
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            width: 60,
                            height: 40,
                            padding: const EdgeInsets.all(10),
                            decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10),
                              ),
                            ),
                            child:
                                SvgPicture.asset("assets/icons/Icon-cross.svg"),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(right: 20, left: 20, top: 70),
                      child: SvgPicture.asset(
                        "assets/images/Secure.svg",
                        height: 160,
                        width: 170,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: double.infinity,
                height: size.height,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: size.height * .1,
                      ),
                      Container(
                        padding: const EdgeInsets.only(
                          bottom: 4,
                        ),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                          color: AppTheme.primaryColor,
                          width: 2, // This would be the width of the underline
                        ))),
                        child: Text("Reset Password",
                            style: AppTheme.blackHeadline),
                      ),
                      const SizedBox(height: 20),
                      CustomFormField(
                        controller: email,
                        prefixIcon: const Icon(
                          Icons.email,
                          color: Colors.white,
                        ),
                        label: "Email",
                        hintText: "Enter Email",
                        keyboardType: TextInputType.emailAddress,
                        validator: (value) => Validator.validateEmail(value!),
                      ),
                      SizedBox(
                        height: size.height * .1,
                      ),
                      DefaultButton(
                        text: "Reset Password",
                        press: () async {
                          reset();
                        },
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
