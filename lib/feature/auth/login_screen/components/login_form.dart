// ignore_for_file: library_private_types_in_public_api, avoid_print, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

import '../../../../../redux/action_report.dart';
import '../../../../../redux/app/app_state.dart';
import '../../../../../utils/progress_dialog.dart';
import '../../../../../utils/validator.dart';

import '../../../../utils/app_theme.dart';
import '../../../../utils/strings.dart';
import '../../../presentations/users_screens/admin_screens/main_view.dart';
import '../../../widgets/custom_form_field.dart';
import '../../../widgets/custom_text_button.dart';
import '../../../widgets/password_form_field.dart';
import '../../auth_view_model.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      builder: (_, viewModel) => LoginFormContent(
        viewModel: viewModel,
      ),
      converter: (store) {
        return AuthViewModel.fromStore(store);
      },
    );
  }
}

class LoginFormContent extends StatefulWidget {
  final AuthViewModel? viewModel;

  const LoginFormContent({
    Key? key,
    this.viewModel,
  }) : super(key: key);
  @override
  _LoginFormContentState createState() => _LoginFormContentState();
}

class _LoginFormContentState extends State<LoginFormContent> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  ProgressDialog? progressDialog;
  bool _rememberMe = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didUpdateWidget(LoginFormContent oldWidget) {
    super.didUpdateWidget(oldWidget);
    Future.delayed(Duration.zero, () {
      if (widget.viewModel!.getLoginReport?.status == ActionStatus.running) {
        progressDialog ??= ProgressDialog(context);

        if (!progressDialog!.isShowing()) {
          progressDialog!.setMessage("Login...");
          progressDialog!.show();
        }
      } else if (widget.viewModel!.getLoginReport?.status ==
          ActionStatus.error) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        showTopSnackBar(
          context as OverlayState,
          CustomSnackBar.error(
            message: widget.viewModel!.getLoginReport!.msg.toString(),
          ),
        );
      } else if (widget.viewModel!.getLoginReport?.status ==
          ActionStatus.complete) {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (_) => const MainView()),
            (route) => false);
        widget.viewModel!.getLoginReport?.status = null;
      } else {
        if (progressDialog != null && progressDialog!.isShowing()) {
          progressDialog!.hide();
          progressDialog = null;
        }
      }
    });
  }

  Widget _buildEmailTF() {
    return CustomFormField(
      label: 'Email',
      controller: email,
      hintText: 'Enter your Email',
      keyboardType: TextInputType.emailAddress,
      validator: (value) => Validator.validateEmail(value!),
      prefixIcon: const Icon(
        Icons.email,
        color: Colors.white,
      ),
    );
  }

  Widget _buildPasswordTF() {
    return PasswordFormField(
      label: "Password",
      controller: password,
      hintText: 'Enter your Password',
      validator: (value) => Validator.validatePassword(value!),
      prefixIcon: const Icon(
        Icons.lock,
        color: Colors.white,
      ),
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () => Navigator.pushNamed(context, "/reset_password"),
        child: Text(
          "Forget Password",
          style: AppTheme.whiteText,
        ),
      ),
    );
  }

  Widget _buildRememberMeCheckbox() {
    return SizedBox(
      height: 30.0,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(unselectedWidgetColor: Colors.white),
            child: Checkbox(
              value: _rememberMe,
              checkColor: Colors.green,
              activeColor: Colors.white,
              onChanged: (value) {
                setState(() {
                  _rememberMe = value!;
                });
              },
            ),
          ),
          Text(
            "Remember me",
            style: AppTheme.whiteText,
          ),
        ],
      ),
    );
  }

  void login() async {
    if (email.text.isEmpty) {
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: "Email is empty",
        ),
      );
      return;
    }
    if (password.text.isEmpty) {
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: "password is empty",
        ),
      );
      return;
    }

    if (_formKey.currentState!.validate()) {
      await widget.viewModel?.login!(email.text, password.text);
    } else {
      // ignore: avoid_print
      print("Not Valid");
      showTopSnackBar(
        context as OverlayState,
        const CustomSnackBar.error(
          message: "your Email not valid",
        ),
      );
    }
  }

  Widget _buildLoginBtn() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20),
      child: CustomTextButton(
        onPressed: login,
        text: "Login",
      ),
    );
  }

  Widget _buildSignUpWithText() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 25),
      child: Column(
        children: [
          Text(
            "__OR__",
            style: AppTheme.whiteText.copyWith(fontSize: 18),
          ),
          const SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "you don't have an account ",
                style: AppTheme.whiteText
                    .copyWith(fontSize: 16, color: AppTheme.backgroundColor),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, signUpScreen);
                },
                child: Text(
                  " Sign up",
                  style: AppTheme.whiteText.copyWith(fontSize: 18),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Column(
            children: [
              _buildEmailTF(),
              _buildPasswordTF(),
              _buildForgotPasswordBtn(),
              _buildRememberMeCheckbox(),
              _buildLoginBtn(),
              _buildSignUpWithText(),
            ],
          ),
        ));
  }
}
