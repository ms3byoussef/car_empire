import 'package:flutter/material.dart';

const String rent = "Rent";
const String buy = "Buy";
const String sell = "Sell";
const List<String> carCategory = [
  "Sell",
  "Rent",
];
const List<String> budgetType = [
  "less 1500 KWD",
  "1500 to 3000",
  "bigger 3000",
];
const List<String> fuelType = [
  "Gasoline / petrol",
  "Diesel fuel",
  "Hybrid",
  "Electric"
];
const List<String> yesOrNo = ["Yes", "No"];
const List<String> nums = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
List<String> images = [
  "assets/images/Buy.jpg",
  "assets/images/Sell.jpg",
  "assets/images/Rent.jpg",
];
List<String> carBrands = [
  'Acura',
  'Alfa Romeo',
  'Audi',
  'BMW',
  'Bentley',
  'Buick',
  'Cadillac',
  'Chevrolet',
  'Chrysler',
  'Dodge',
  'Fiat',
  'Ford',
  'GMC',
  'Genesis',
  'Honda',
  'Hyundai',
  'Infinite',
  'Jaguar',
  'Jeep',
  'Kia',
  'Land Rover',
  'Lexus',
  'Lincoln',
  'Lotus',
  'Lucid',
  'Maserati',
  'Mazda',
  'Mercedes-Benz',
  'Mercury',
  'Mini',
  'Nissan',
  'Polestar',
  'Pontiac',
  'Porsche',
  'Ram',
  'Vivian',
  'Rolls-Royce',
  'Saab',
  'Saturn',
  'Scion',
  'Scout',
  'Smart',
  'Subaru',
  'Suzuki',
  'Tesla',
  'Toyota',
  'VinFast',
  'Volkswagen',
  'Volvo',
];
List<String> carCategoryForClient = ["Sell"];
List<String> carCategoryForAdmin = [
  "Sell",
  "Rent",
];
const List<String> transmissionsTypes = [
  "Automatic ",
  "Manual",
  "Automated Manual",
  "Continuously Variable"
];

List<String> carBody = [
  'Sports Cars',
  'Sedan',
  'Coupe',
  'Hatchback	',
  'Convertible',
  'Sport-utility vehicle (SUV)',
  'Pickup truck',
  'Jeep',
  'Electric car'
      'Spyder',
  'Hot',
  'hatch',
  'Limousine',
  'Pony car',
  'Sports sedan',
  'Military vehicle'
];
List<Color> carColors = [
  Colors.black,
  Colors.white,
  Colors.red,
  Colors.yellow,
  Colors.blue,
  Colors.grey,
  Colors.cyan,
  Colors.green,
  Colors.orange,
  Colors.blueGrey,
];
