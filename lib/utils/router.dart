import 'package:flutter/material.dart';

import '../feature/auth/login_screen/login_screen.dart';
import '../feature/auth/reset_password/reset_password.dart';
import '../feature/auth/sign_up_screen/sign_up_screen.dart';
import '../feature/onboarding_screens/intro_screen/intro_screen.dart';
import '../feature/onboarding_screens/splash_screen/splash_screen.dart';
import '../feature/onboarding_screens/welcome_screen/welcome_screen.dart';
import '../feature/presentations/users_screens/components/edit_profile/edit_profile_screen.dart';
import '../feature/presentations/users_screens/components/profile/profile_screen.dart';
import 'strings.dart';

class AppRouter {
  static Map<String, WidgetBuilder> get routes {
    return <String, WidgetBuilder>{
      welcomeScreen: (_) => const WelcomeScreen(),
      introScreen: (_) => const IntroScreen(),
      loginScreen: (_) => const LoginScreen(),
      signUpScreen: (_) => const SignUpScreen(),
      editProfileScreen: (_) => const EditProfileScreen(),
      // editCarScreen: (_) => const EditCarScreen(),
      profileScreen: (_) => const ProfileScreen(),
      splashScreen: (_) => const SplashScreen(),
      resetPasswordScreen: (_) => const ResetPassword(),
    };
  }
}
