import 'car_details_model.dart';

class CarModel {
  String id;
  String? ownerID;
  String? title;
  String? description;
  int? price;
  String? category;
  String? carImage;
  String? status;
  CarDetailsModel? carDetails;

  CarModel({
    this.id = "",
    this.ownerID,
    this.title,
    this.description,
    this.price,
    this.category,
    this.carImage,
    this.carDetails,
    this.status,
  });

  CarModel.fromJson(Map<dynamic, dynamic> map)
      : id = map['id'],
        title = map['title'],
        price = map['price'],
        category = map['category'],
        ownerID = map['ownerID'],
        carImage = map['carImage'],
        status = map['status'],
        description = map['description'],
        carDetails = CarDetailsModel.fromJson(map['carDetails']);

  Map<String, Object?> toJson() => {
        'id': id,
        'title': title,
        'price': price,
        'category': category,
        'ownerID': ownerID,
        'carImage': carImage,
        'status': status,
        'description': description,
        'carDetails': carDetails!.toJson(),
      };

  CarModel copyWith({
    String id = "",
    String? title,
    int? price,
    String? category,
    String? carImage,
    String? status,
    String? description,
    CarDetailsModel? carDetails,
  }) {
    return CarModel(
      id: id,
      title: title,
      price: price,
      category: category,
      ownerID: ownerID,
      carImage: carImage,
      status: status,
      description: description,
      carDetails: carDetails,
    );
  }
}
