class UserInformation {
  final String? isMarried;
  final String? isSportive;
  final String? economic;
  final String? likeLuxury;
  final String? numInFamily;
  final String? budget;

  UserInformation({
    this.isMarried,
    this.isSportive,
    this.economic,
    this.likeLuxury,
    this.numInFamily,
    this.budget,
  });

  UserInformation.fromJson(Map<dynamic, dynamic> map)
      : isMarried = map['isMarried'] ?? "",
        isSportive = map['isSportive'] ?? "",
        economic = map['economic '] ?? "",
        likeLuxury = map['likeLuxury'] ?? "",
        numInFamily = map['numInFamily'] ?? "",
        budget = map['budget'] ?? "";

  Map<String, dynamic> toJson() => {
        'isMarried': isMarried ?? "",
        'isSportive': isSportive ?? "",
        'economic ': economic ?? "",
        'likeLuxury': likeLuxury ?? "",
        'numInFamily': numInFamily ?? "",
        'budget': budget ?? "",
      };

  UserInformation copyWith({
    final String? isMarried,
    final String? isSportive,
    final String? economic,
    final String? likeLuxury,
    final String? numInFamily,
    final String? budget,
  }) {
    return UserInformation(
      isMarried: isMarried ?? this.isMarried,
      isSportive: isSportive ?? this.isSportive,
      economic: economic ?? this.economic,
      likeLuxury: likeLuxury ?? this.likeLuxury,
      numInFamily: numInFamily ?? this.numInFamily,
      budget: budget ?? this.budget,
    );
  }
}
