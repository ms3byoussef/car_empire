import 'car_model.dart';

class RequestModel {
  String id;
  String? userID;
  CarModel? car;
  String? orderTime;
  String? status;

  RequestModel({
    this.id = "",
    this.userID,
    this.car,
    this.orderTime,
    this.status,
  });

  RequestModel.fromJson(Map<dynamic, dynamic> map)
      : id = map['id'],
        userID = map['userID'],
        car = CarModel.fromJson(map['car']),
        orderTime = map['orderTime'],
        status = map['status'];

  Map<dynamic, dynamic> toJson() => {
        'id': id,
        'userID': userID,
        'car': car!.toJson(),
        'orderTime': orderTime,
        'status': status,
      };

  RequestModel copyWith({
    String id = "",
    String? userID,
    CarModel? car,
    String? orderTime,
    String? status,
  }) {
    return RequestModel(
      id: id,
      userID: userID,
      car: car,
      orderTime: orderTime,
      status: status,
    );
  }
}
