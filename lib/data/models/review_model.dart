class ReviewModel {
  String? id;
  String? userId;
  String? review;
  String? rate;

  ReviewModel({this.id, this.userId, this.review, this.rate});

  ReviewModel.fromJson(Map<dynamic, dynamic> map)
      : id = map['id'] ?? "",
        userId = map['userId'] ?? "",
        review = map['review'] ?? "",
        rate = map['rate'] ?? "";

  Map<String, dynamic> toJson() => {
        'id': id,
        'userId': userId,
        'review': review,
        'rate': rate,
      };

  ReviewModel copyWith({
    String? id,
    String? userId,
    String? review,
    String? rate,
  }) {
    return ReviewModel(
      id: id ?? this.id,
      userId: userId ?? this.userId,
      review: review ?? this.review,
      rate: rate ?? this.rate,
    );
  }
}
