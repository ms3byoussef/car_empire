class CarDetailsModel {
  final String? brand;
  final String? modelYear;
  final String? modelBody;
  final String? fuel;
  final String? engine;
  final String? transmission;
  final String? numberOfAirbag;
  final String? numberOfGears;
  final String? numberOfSeating;
  final List<int>? colors;
  CarDetailsModel({
    this.brand,
    this.modelYear,
    this.modelBody,
    this.fuel,
    this.engine,
    this.transmission,
    this.numberOfAirbag,
    this.numberOfGears,
    this.numberOfSeating,
    this.colors,
  });
  CarDetailsModel.fromJson(Map<dynamic, dynamic> map)
      : brand = map['brand'],
        modelYear = map['modelYear'],
        modelBody = map['modelBody'],
        fuel = map['fuel'],
        engine = map['engine'],
        transmission = map['transmission'],
        // fuel = map['companyID'], modelYear = map['modelYear'],
        // modelBody = map['price'],
        // fuel = map['companyID'], modelYear = map['modelYear'],
        colors =
            map['colors'] == null ? [] : List.of(map['colors'].cast<int>()),
        numberOfAirbag = map['numberOfAirbag'],
        numberOfGears = map['numberOfGears'],
        numberOfSeating = map['numberOfSeating'];

  Map<String, Object?> toJson() => {
        if (brand != null) 'brand': brand,
        if (modelYear != null) 'modelYear': modelYear,
        if (modelBody != null) 'modelBody': modelBody,
        if (fuel != null) 'fuel': fuel,
        if (engine != null) 'engine': engine,
        if (transmission != null) 'transmission': transmission,
        if (numberOfAirbag != null) 'numberOfAirbag': numberOfAirbag,
        if (numberOfGears != null) 'numberOfGears': numberOfGears,
        if (numberOfSeating != null) 'numberOfSeating': numberOfSeating,
        if (colors != null)
          'colors': colors?.map((e) => e.toInt()).toList() ?? [],
      };

  CarDetailsModel copyWith({
    final String? brand,
    final String? modelYear,
    final String? modelBody,
    final String? fuel,
    final String? engine,
    final String? transmission,
    final String? numberOfAirbag,
    final String? numberOfGears,
    final String? numberOfSeating,
    final List<int>? colors,
  }) {
    return CarDetailsModel(
      brand: brand ?? this.brand,
      modelYear: modelYear ?? this.modelYear,
      modelBody: modelBody ?? this.modelBody,
      fuel: fuel ?? this.fuel,
      engine: engine ?? this.engine,
      transmission: transmission ?? this.transmission,
      numberOfAirbag: numberOfAirbag ?? this.numberOfAirbag,
      numberOfGears: numberOfGears ?? this.numberOfGears,
      numberOfSeating: numberOfSeating ?? this.numberOfSeating,
      colors: colors ?? this.colors,
    );
  }
}
