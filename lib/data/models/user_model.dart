import 'package:car_empire/data/models/user_info.dart';

class UserModel {
  String? id;
  String? name;
  String? phone;
  String? email;
  String? address;
  String? photo;
  String role;
  UserInformation? userInfo;

  UserModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.address,
    this.photo,
    this.role = "client",
    this.userInfo,
  });

  UserModel.fromJson(Map<dynamic, dynamic> map)
      : id = map['id'] ?? "",
        name = map['name'] ?? "",
        email = map['email'] ?? "",
        phone = map['phone'] ?? "",
        address = map['address'] ?? "",
        photo = map["photo"] ?? "",
        userInfo = UserInformation.fromJson(map["userInfo"] ?? {}),
        role = map['role'] ?? "";

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'email': email,
        'phone': phone,
        'address': address,
        'photo': photo,
        'role': role,
        if (userInfo != null) 'userInfo': userInfo?.toJson() ?? {},
      };

  UserModel copyWith({
    String? id,
    String? name,
    String? email,
    String? phone,
    String? address,
    String? photo,
    String? role,
    UserInformation? userInfo,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      email: email ?? this.email,
      phone: phone ?? this.phone,
      address: address ?? this.address,
      photo: photo ?? this.photo,
      role: role ?? this.role,
      userInfo: userInfo ?? this.userInfo,
    );
  }
}
