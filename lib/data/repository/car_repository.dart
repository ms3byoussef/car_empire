import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

import '../models/car_model.dart';

class CarRepository {
  const CarRepository();
  Future<CarModel> postCar(CarModel carModel, File? img) async {
    if (img == null) {
      carModel.carImage = "";
    } else {
      firebase_storage.FirebaseStorage storage =
          firebase_storage.FirebaseStorage.instance;
      firebase_storage.Reference ref =
          storage.ref().child("cars_images").child(img.toString());
      final uploadTask = ref.putFile(img);

      // ignore: avoid_print
      await uploadTask.whenComplete(() => print("complategetDownloadURL"));
      carModel.carImage = await ref.getDownloadURL();
    }

    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('cars').push();

    carModel.id = databaseReference.key!;

    databaseReference.set(carModel.toJson());
    var carSnapshot = await databaseReference.once();
    carModel.id = carSnapshot.snapshot.key!;

    Map<dynamic, dynamic> map =
        carSnapshot.snapshot.value as Map<dynamic, dynamic>;
    CarModel car = CarModel.fromJson(map);

    return car;
  }

  Future<List<CarModel>> getCars() async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("cars");
    var carSnapshot = await databaseReference.once();
    List<CarModel> cars = [];

    Map<dynamic, dynamic> map =
        carSnapshot.snapshot.value as Map<dynamic, dynamic>;

    map.forEach((key, value) async {
      value["id"] = key;
      CarModel car = CarModel.fromJson(value);

      cars.add(car);
    });

    return cars;
  }

  Future<List<CarModel>> getCategoryCars(String category) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("cars");
    var carSnapshot = await databaseReference.once();
    List<CarModel> cars = [];

    Map<dynamic, dynamic> map =
        carSnapshot.snapshot.value as Map<dynamic, dynamic>;

    map.forEach((key, value) async {
      value["id"] = key;
      CarModel car = CarModel.fromJson(value);
      if (car.category == category) {
        cars.add(car);
      }
    });

    return cars;
  }

  Future<CarModel> editCar(
    CarModel? carModel, {
    File? img,
  }) async {
    if (img != null) {
      firebase_storage.FirebaseStorage storage =
          firebase_storage.FirebaseStorage.instance;
      firebase_storage.Reference ref =
          storage.ref().child("cars_images").child(img.toString());
      final uploadTask = ref.putFile(img);

      // ignore: avoid_print
      await uploadTask.whenComplete(() => print("complategetDownloadURL"));
      carModel!.carImage = await ref.getDownloadURL();
    } else {}

    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("cars/${carModel!.id}");
    databaseReference.update(
      carModel.toJson(),
    );
    var carSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        carSnapshot.snapshot.value as Map<dynamic, dynamic>;
    CarModel car = CarModel.fromJson(map);
    car.id = carSnapshot.snapshot.key!;

    return car;
  }

  Future<List<CarModel>> searchCarsResult(String text) async {
    final rep = await FirebaseDatabase.instance.ref().child("cars").once();
    List<CarModel> cars = [];
    Map<dynamic, dynamic> values = rep.snapshot.value as Map<dynamic, dynamic>;

    values.forEach((key, value) {
      if (value['name'].toLowerCase().contains(text.toLowerCase())) {
        value["id"] = key;
        cars.add(CarModel.fromJson(value));
      }
    });

    return cars;
  }

  removeCar(
    String carId,
  ) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("cars/$carId");

    databaseReference.remove();
  }
}
