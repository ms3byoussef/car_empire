import 'dart:convert';

import 'package:car_empire/data/models/user_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/car_model.dart';
import '../models/request_model.dart';
import '../models/review_model.dart';
import '../models/user_model.dart';

class ClientRepository {
  const ClientRepository();

  Future<UserModel?> updateUserInfo(UserInformation userInfo) async {
    UserModel user = UserModel();

    var userAuth = (FirebaseAuth.instance.currentUser);
    user.id = userAuth!.uid;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var usersReferences =
        FirebaseDatabase.instance.ref().child("users/${userAuth.uid}");

    await usersReferences.update({"userInfo": userInfo.toJson()});

    var userSnapshot = await usersReferences.once();
    Map<dynamic, dynamic> map =
        userSnapshot.snapshot.value as Map<dynamic, dynamic>;
    user = UserModel.fromJson(map);
    user.id = userAuth.uid;
    const JsonEncoder encoder = JsonEncoder();

    sharedPreferences.setString("users", encoder.convert((user.toJson())));

    return user;
  }

  Future<RequestModel> postRequest(RequestModel request) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('requests').push();
    request.id = databaseReference.key!;

    databaseReference.set(request.toJson());
    var requestSnapshot = await databaseReference.once();

    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    RequestModel requestModel = RequestModel.fromJson(map);
    requestModel.id = requestSnapshot.snapshot.key!;
    return requestModel;
  }

  Future<List<RequestModel>> getRequests() async {
    List<RequestModel> requests = [];
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("requests");
    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    map.forEach((key, value) async {
      value["id"] = key;
      RequestModel request = RequestModel.fromJson(value);

      requests.add(request);
    });

    return requests;
  }

  Future<UserModel> getRequestUser(RequestModel request) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('users/${request.userID}');

    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    UserModel requestUser = UserModel.fromJson(map);
    requestUser.id = requestSnapshot.snapshot.key!;
    return requestUser;
  }

  Future<UserModel> getCarOwnerUser(CarModel carModel) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('users/${carModel.ownerID}');

    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    UserModel company = UserModel.fromJson(map);
    company.id = requestSnapshot.snapshot.key!;
    return company;
  }

  Future<CarModel> getCarInRequest(RequestModel request) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('cars/${request.car!.id}');

    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    CarModel carInRequest = CarModel.fromJson(map);
    carInRequest.id = requestSnapshot.snapshot.key!;

    return carInRequest;
  }

  Future<RequestModel> updateRequest(
      {String? requestID, String? status}) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("requests/$requestID");

    databaseReference.update({"status": status});
    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    RequestModel requestModel = RequestModel.fromJson(map);
    requestModel.id = requestSnapshot.snapshot.key!;

    return requestModel;
  }

  Future<CarModel> updateCarStatus({String? status, String? carID}) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("cars/$carID");

    databaseReference.update({"status": status});
    var requestSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        requestSnapshot.snapshot.value as Map<dynamic, dynamic>;
    CarModel carModel = CarModel.fromJson(map);
    carModel.id = requestSnapshot.snapshot.key!;

    return carModel;
  }

  removeRequest(RequestModel request) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("requests/${request.id}");

    databaseReference.remove();
  }

  Future<ReviewModel> postReview(ReviewModel review) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('reviews').push();
    review.id = databaseReference.key;

    databaseReference.set(review.toJson());
    var reviewSnapshot = await databaseReference.once();
    review.id = reviewSnapshot.snapshot.key!;
    Map<dynamic, dynamic> map =
        reviewSnapshot.snapshot.value as Map<dynamic, dynamic>;

    ReviewModel reviewModel = ReviewModel.fromJson(map);
    reviewModel.id = reviewSnapshot.snapshot.key!;

    return reviewModel;
  }

  Future<List<UserModel>> getClients() async {
    // print("getEmployees");
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("users");
    var serviceSnapshot = await databaseReference.once();
    List<UserModel> clients = [];

    Map<dynamic, dynamic> map =
        serviceSnapshot.snapshot.value as Map<dynamic, dynamic>;
    map.forEach((key, value) async {
      if (value['role'] == "client") {
        UserModel client = UserModel.fromJson(value);
        client.id = key;
        clients.add(client);
        // print(map);
      }
    });

    return clients;
  }

  Future<UserModel> getReviewUser(ReviewModel review) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child('users/${review.userId}');

    var orderSnapshot = await databaseReference.once();
    Map<dynamic, dynamic> map =
        orderSnapshot.snapshot.value as Map<dynamic, dynamic>;
    UserModel reviewUser = UserModel.fromJson(map);
    reviewUser.id = orderSnapshot.snapshot.key!;
    return reviewUser;
  }

  Future<List<ReviewModel>> getReviews() async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("reviews");

    var reviewSnapshot = await databaseReference.once();
    List<ReviewModel> reviews = [];

    Map<dynamic, dynamic> map =
        reviewSnapshot.snapshot.value as Map<dynamic, dynamic>;
    map.forEach((key, value) async {
      value["id"] = key;
      ReviewModel review = ReviewModel.fromJson(value);

      reviews.add(review);
    });

    return reviews;
  }

  removeReview(ReviewModel review) async {
    DatabaseReference databaseReference =
        FirebaseDatabase.instance.ref().child("review/${review.id}");

    databaseReference.remove();
  }
}
