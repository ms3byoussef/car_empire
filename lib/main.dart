import 'package:car_empire/utils/router.dart';
import 'package:car_empire/utils/app_theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'redux/app/app_state.dart';
import 'redux/store.dart';
import 'utils/strings.dart';

void main() async {
  var store = await createStore();

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    MyApp(
      store: store,
    ),
  );
}

class MyApp extends StatefulWidget {
  final Store<AppState> store;

  const MyApp({required this.store, Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: widget.store,
      child: MaterialApp(
        title: 'car empire',
        // builder: DevicePreview.appBuilder,
        debugShowCheckedModeBanner: false,
        theme: myTheme(),
        routes: AppRouter.routes,
        initialRoute: splashScreen,
      ),
    );
  }
}
